/*
 * data.h
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DATA_H_
#define _DATA_H_

#include <iostream>
#include <vector>
#include <unordered_set>
#include <string>
#include <SDL2/SDL.h>

typedef struct {
  SDL_Colour sky;
  SDL_Colour word;
  SDL_Colour match;
  SDL_Colour score;
} GameColours;

typedef struct {
  int maxwrds; // max number of words on screen
  int falldelay; // delay between each new word
  int length; // word's length
  int bossmode; // number of words before entering in boss mode
  int nextlevel; // number of words before next level
} Difficulty;

class Data {

  friend inline std::istream& operator>>(std::istream& is, std::vector<std::vector<std::string>*>& vec);
  
 public:
  Data();
  ~Data();
  void load_dictionary(const std::string& path);
  const GameColours* get_game_colours(unsigned short index) const;
  inline void clear(void) { container.clear(); }
  inline int size(void) const { return container.size(); }
  void clear(unsigned short int wl);
  void push(unsigned short index);
  std::string pop(void);
  Difficulty get_difficulty(int index);
  std::string get_debug_info(void);

 protected:
  void load_vector(const std::string& path, std::vector<std::vector<std::string>*>* vec);
  std::vector<GameColours> colours;
  std::vector<Difficulty> flow;
  std::vector<std::vector<std::string>*> words;
  std::unordered_set<std::string> container;
  std::vector<std::string> one;
  std::vector<std::string> two;
  std::vector<std::string> three;
  std::vector<std::string> four;
  std::vector<std::string> five;
  std::vector<std::string> six;
  std::vector<std::string> seven;
  std::vector<std::string> eight;
  std::vector<std::string> nine;
  std::vector<std::string> ten;
};

#endif // _DATA_H_
