/*
 * cloud.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cloud.h"
#include <chrono>
#include <random>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "fps.h"

int Cloud::width = 1024;
int Cloud::height = 800;

Cloud::Cloud(SDL_Renderer* rndr, SDL_Colour clr, unsigned int alp) : renderer (rndr), colour (clr), alpha (alp), speed (0.0f), acceleration (0.0f) {
  texture = create_texture(renderer, colour, &source, &ellipses);
  destination = source;
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_int_distribution<int> coord_y(0, height * 0.8f);
  destination.x = -destination.w;
  destination.y = coord_y(generator);
  animation.set_total_frames(16);
  animation.set_frame_rate(250);
  to_red = true;
  animating = false;
}

Cloud::~Cloud() {
  if (texture) {
    SDL_DestroyTexture(texture);
  }
}

void Cloud::draw(void) const {
  SDL_Rect dest = destination;
  dest.x += (dest.w / 2) - (source.w / 2);
  dest.y += (dest.h / 2) - (source.h / 2);
  dest.w = source.w;
  dest.h = source.h;
  if (texture != nullptr) {
    SDL_SetTextureAlphaMod(texture, alpha);
    SDL_RenderCopy(renderer, texture, &source, &dest);
  }
}

void Cloud::on_loop(void) {
  if (animating) {
    animate_colour();
  }
  
  if (destination.x > width) {
    speed = 0.0f;
    acceleration = 0.0f;
  } else {
    acceleration = 0.5f;
  }

  speed += acceleration * Fps::control.get_speed_factor();

  if (speed > MAX_SPEED) {
    speed = MAX_SPEED;
  }

  move(speed);
}

void Cloud::move(float spd) {
  if (spd == 0.0f) {
    return;
  }

  spd *= Fps::control.get_speed_factor();
  destination.x += round(spd);
}

void Cloud::rebuild_texture(SDL_Colour clr) {
  SDL_SetRenderTarget(renderer, texture);
  SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
  SDL_SetRenderDrawColor(renderer, 0,0,0,0);
  SDL_RenderClear(renderer);
  std::vector<SDL_Rect>::iterator iter = ellipses.begin();
  while (iter != ellipses.end()) {
    filledEllipseRGBA(renderer, iter->x, iter->y, iter->w / 2, iter->h / 2, clr.r, clr.g, clr.b, clr.a);
    iter++;
  }
  SDL_SetRenderTarget(renderer, nullptr);
}

void Cloud::start_colour_animation(bool flag) {
  to_red = flag;
  animating = true;
  animation.set_current_frame(0);
}

bool Cloud::animate_colour(void) {
  if (animation.animate()) {
    int cf = animation.get_current_frame();
    SDL_Colour clr;
    if (to_red) {
      clr = WHITE;
      clr.g -= 0x0a * cf;
      clr.b -= 0x0a * cf;
    } else {
      clr = RED;
      clr.g += 0x0a * cf;
      clr.b += 0x0a * cf;
    }
    rebuild_texture(clr);
    bool status = cf < animation.get_total_frames();
    if (!status) {
      animating = false;
    }
    return status;
  }
  return true;
}

SDL_Texture* Cloud::create_texture(SDL_Renderer* rndr, SDL_Colour clr, SDL_Rect* rct, std::vector<SDL_Rect>* ellipses) {
  SDL_Texture* txtr = nullptr;
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_int_distribution<int> texture_width(width * 0.20f, width * 0.40f);
  std::uniform_int_distribution<int> texture_height(height * 0.06f, height * 0.10f);
  *rct = { 0, 0, texture_width(generator), texture_height(generator) };
  txtr = SDL_CreateTexture(rndr, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, rct->w, rct->h);
  if (txtr) {
    SDL_SetRenderTarget(rndr, txtr);
    SDL_SetTextureBlendMode(txtr, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(rndr, 0,0,0,0);
    SDL_RenderClear(rndr);
    //SDL_SetRenderDrawColor(rndr, 0x01, 0x01, 0x01, 0xff);
    //SDL_RenderDrawRect(rndr, rct);
    std::uniform_int_distribution<int> ellipse_width(rct->w * 0.5f, rct->w);
    std::uniform_int_distribution<int> ellipse_height(rct->h * 0.4f, rct->h);
    std::uniform_int_distribution<int> loops(4, 15);
    for (int i = 0; i < loops(generator); i++) {
      SDL_Rect dst = { 0, 0, ellipse_width(generator), ellipse_height(generator) };
      std::uniform_int_distribution<int> ellipse_x(dst.w / 2, rct->w - (dst.w / 2));
      std::uniform_int_distribution<int> ellipse_y(dst.h / 2, rct->h - (dst.h / 2));
      dst.x = ellipse_x(generator);
      dst.y = ellipse_y(generator);
      ellipses->push_back(dst);
      filledEllipseRGBA(rndr, dst.x, dst.y, dst.w / 2, dst.h / 2, clr.r, clr.g, clr.b, clr.a);
    }
    SDL_SetRenderTarget(rndr, nullptr);
  }
  return txtr;
}
