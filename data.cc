/*
 * data.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "data.h"
#include <chrono>
#include <random>
#include <iterator>
#include <fstream>
#include <sstream>

inline std::istream& operator>>(std::istream& is, std::vector<std::vector<std::string>*>& vec) {
  while (is.good()) {
    std::string str;
    is >> str;
    if (is.good()) {
      int length = str.size();
      if (length <= vec.size()) {
	vec.at(length - 1)->push_back(str);
      }
    }
  }
  return is;
}

Data::Data() {
  colours = {  //    sky,                  word,                 match,                score
	     { {0xff,0xcc,0xcc,0xff},{0x1a,0x00,0x00,0xff},{0xee,0x00,0x00,0xff},{0x00,0x00,0xaa,0xff} }, // level 1
	     { {0xff,0xe6,0xcc,0xff},{0x1a,0x0d,0x00,0xff},{0xee,0x77,0x00,0xff},{0x00,0x00,0xaa,0xff} }, // level 2
	     { {0xff,0xff,0xcc,0xff},{0x1a,0x1a,0x00,0xff},{0xee,0x77,0x00,0xff},{0x00,0x00,0xaa,0xff} }, // level 3
	     { {0xe6,0xff,0xcc,0xff},{0x0d,0x1a,0x00,0xff},{0xee,0xee,0x00,0xff},{0x00,0x00,0xaa,0xff} }, // level 4
	     { {0xcc,0xff,0xcc,0xff},{0x00,0x1a,0x00,0xff},{0x00,0xee,0x00,0xff},{0x00,0x00,0xaa,0xff} }, // level 5
	     { {0xcc,0xff,0xe6,0xff},{0x00,0x1a,0x0d,0xff},{0x00,0xee,0x77,0xff},{0x00,0x00,0xaa,0xff} }, // level 6
	     { {0xcc,0xff,0xff,0xff},{0x00,0x1a,0x1a,0xff},{0x00,0xee,0xee,0xff},{0x00,0x00,0xaa,0xff} }, // level 7
	     { {0xcc,0xe6,0xff,0xff},{0x00,0x0d,0x1a,0xff},{0x00,0x77,0xee,0xff},{0x00,0x00,0xaa,0xff} }, // level 8
	     { {0xcc,0xcc,0xff,0xff},{0x00,0x00,0x1a,0xff},{0x00,0x00,0xee,0xff},{0x00,0x00,0xaa,0xff} }, // level 9
	     { {0xe6,0xcc,0xff,0xff},{0x0d,0x00,0x1a,0xff},{0x77,0x00,0xee,0xff},{0x00,0x00,0xaa,0xff} }, // level 10
	     { {0xff,0xcc,0xff,0xff},{0x1a,0x00,0x1a,0xff},{0xee,0x00,0xee,0xff},{0x00,0x00,0xaa,0xff} }, // level 11
	     { {0xff,0xcc,0xe6,0xff},{0x1a,0x00,0x0d,0xff},{0xee,0x00,0x77,0xff},{0x00,0x00,0xaa,0xff} }  // level 12
	     
  };
  // { max words on screen, new word delay, max word's size, number of word before boss, number of word before next level }
  flow = {
	  {  5, 1500, 1,  0,   10  }, // level 1
	  {  6, 1490, 2,  20,  30  }, // level 2
	  {  7, 1480, 3,  50,  65  }, // level 3
	  {  8, 1470, 4,  90,  105 }, // level 4
	  {  9, 1480, 5,  140, 160 }, // level 5
	  { 10, 1490, 6,  200, 220 }, // level 6
	  {  9, 1500, 7,  260, 290 }, // level 7
	  {  8, 1520, 8,  330, 350 }, // level 8
	  {  7, 1540, 9,  390, 410 }, // level 9
	  {  6, 1560, 10, 450, 470 }, // level 10
	  {  6, 1580, 10, 510, 530 }, // level 11
	  {  6, 1600, 10, 570, 590 }  // level 12
  };
}

Data::~Data() {
  
}

void Data::load_dictionary(const std::string& path) {
  words.push_back(&one);
  words.push_back(&two);
  words.push_back(&three);
  words.push_back(&four);
  words.push_back(&five);
  words.push_back(&six);
  words.push_back(&seven);
  words.push_back(&eight);
  words.push_back(&nine);
  words.push_back(&ten);
  load_vector(path, &words);
}

const GameColours* Data::get_game_colours(unsigned short index) const {
  if (index > colours.size()) {
    index = colours.size();
  }
  return &colours[index - 1];
}

void Data::clear(unsigned short int wl) {
  std::unordered_set<std::string>::iterator it = container.begin();
  while (it != container.end()) {
    if ((*it).size() <= wl) {
      it = container.erase(it);
    } else {
      it++;
    }
  }
}

void Data::push(unsigned short index) {
  std::vector<std::string>::iterator it = words.at(index - 1)->begin();
  std::vector<std::string>::iterator ed = words.at(index - 1)->end();
  container.insert(it,ed);
}

std::string Data::pop(void) {
  std::string str;
  if (container.size() > 0) {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(0, container.size() - 1);
    int index = distribution(generator);
    std::unordered_set<std::string>::iterator iter = container.begin();
    std::advance(iter, index);
    str = *iter;
    container.erase(iter);
  }
  return str;
}

Difficulty Data::get_difficulty(int index) {
  if (index > flow.size()) {
    index = flow.size();
  }
  return flow.at(index - 1);
}

std::string Data::get_debug_info(void) {
  std::stringstream ss;
  ss << "dictionary content:" << std::endl;
  int n = 1;
  std::vector<std::vector<std::string>*>::iterator iter = words.begin();
  while (iter != words.end()) {
    ss << "size " << n << " = " << (*iter)->size() << std::endl;
    n++;
    iter++;
  }
  return ss.str();
}

void Data::load_vector(const std::string& path, std::vector<std::vector<std::string>*>* vec) {
  std::ifstream infile;
  infile.open(path.c_str(), std::ifstream::in);
  if (infile) {
    infile >> *vec;
    if (infile.fail() && !infile.eof()) {
      std::cerr << "load error file " << path << std::endl;
    }
  } else {
    std::cerr << "unable to open file " << path << std::endl;
  }
  infile.close();
}
