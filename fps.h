/*
 * fps.h
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FPS_H_
#define _FPS_H_

#include <SDL2/SDL.h>

class Fps {
 public:
  static Fps control;

  Fps();
  inline int get_fps(void) const { return num_frames; }
  inline float get_speed_factor(void) const { return speed_factor; }
  void reset(void);
  void on_loop(void);

 private:
  unsigned int old_time;
  unsigned int last_time;
  float speed_factor;
  int num_frames;
  int frames;
  
};

#endif // _FPS_H_
