/*
 * sdlengine.h
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SDLENGINE_H_
#define _SDLENGINE_H_

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <vector>
#include <map>
#include <chrono>
#include <random>
#include <fontconfig/fontconfig.h>
#include "data.h"
#include "text.h"
#include "textentity.h"
#include "cloud.h"

class SDLEngine {

  enum class GameStatus { quit, title, credits, game, pause, gameover, report, ranking };
  
public:

#define SOUNDS_PATH std::string("sounds/")
#define MAX_PLAYER_NAME 10
#define LABEL_LIVES 0
#define LABEL_LEVEL 1
#define LABEL_SCORE 2
#define LABEL_CREDITS 3
#define LABEL_GAMEDESIGN 4
#define LABEL_GDN1 5
#define LABEL_GDN2 6
#define LABEL_PROGRAMMING 7
#define LABEL_PN1 8
#define LABEL_MUSIC 9
#define LABEL_MC1 10
#define LABEL_MC2 11
#define LABEL_MC3 12
#define LABEL_ESC 13
#define LABEL_START 14
#define LABEL_TYPED_CHAR 15
#define LABEL_TYPED_WORD 16
#define LABEL_ACCURACY 17
#define LABEL_TYPE_SPEED 18
#define LABEL_WORDS_PM 19
#define LABEL_BEST_COMBO 20
#define LABEL_WORST_LTR 21
#define LABEL_BEST_LTR 22
#define LABEL_RANKING 23
#define LABEL_RANKING_TOP 24
#define COLOURS_CYCLE_DELAY 80
#define SOUND_KEY     0
#define SOUND_CLEARED 1
#define SOUND_MISTAKE 2
#define SOUND_NEXTLEV 3
#define CHANNEL_KEYBOARD 0
#define CHANNEL_WORD     1
#define CHANNEL_MISTAKE  2
#define CHANNEL_JINGLE   3
  
  typedef struct {
    std::string name;
    int score;
    int level;
    int bestcombo;
    float wordspermin;
  } Record;
  
  SDLEngine(const std::string& p, bool dm = false) : main_path (p), debugmode (dm), window (nullptr), win_width (1024), win_height (800), renderer (nullptr), gamefont (nullptr), bigfont (nullptr), player_rank (0), gamestatus (GameStatus::title) {}
  ~SDLEngine();
  int start(void);
  bool init(void);
  void on_SDL_error(std::ostream &os, const std::string &msg);
  void on_gameover_loop(void);
  void on_title_event(SDL_Event* event);
  void on_credits_event(SDL_Event* event);
  void on_game_event(SDL_Event* event);
  void on_pause_event(SDL_Event* event);
  void on_report_event(SDL_Event* event);
  void on_ranking_event(SDL_Event* event);
  void title_render(void);
  void credits_render(void);
  void game_render(void);
  void report_render(void);
  void ranking_render(void);

protected:
  std::string find_font_path(const std::string& name, const FcChar8* type);
  void cycle_colours(void);
  void init_title(void);
  void init_credits(void);
  void init_game(void);
  void init_report(void);
  bool init_ranking(void);
  std::string create_record_string(const Record& r, int n);
  void load_ranking(void);
  void save_ranking(void);
  void check_words(const std::string& str);
  void reset_words_status(int size);
  void create_score_temp_label(TextEntity* txe);
  void update_colours(void);
  void update_temp_labels(void);
  void update_words(void);
  void update_clouds(void);
  void update_score(void);
  void update_lives(void);
  void get_letters_stats(std::string* worst, std::string* best);
  void update_level(void);
  void start_boss_mode(void);
  void lost_life(const std::string& str, int x);
  void cleanup_data(void);
  void free_memory(void);
  void free_labels(void);
  void free_temp_labels(void);
  void free_words(void);
  void free_sounds(void);
  void free_clouds(void);
  void calculate_coord(int* xc, int* yc, const float* xr, const float* yr, const float* x, const float* y);
  void draw_T(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_i(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_n(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_y(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_e(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_x(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_t(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_G(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_a(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_m(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_O(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_v(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);
  void draw_r(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol);

private:
  std::string main_path;
  SDL_Window* window;
  int win_width;
  int win_height;
  SDL_Renderer* renderer;
  std::vector<Cloud*> clouds;
  long int next_cloud_delay;

  // fonts
  TTF_Font* gamefont;
  TTF_Font* boldfont;
  TTF_Font* bigfont;
  
  // colours cycle
  std::vector<std::pair<SDL_Colour,bool>> colours_cycle;

  // sounds & music
  std::vector<Mix_Chunk*> sounds;
  Mix_Music* game_music;
  
  // game labels
  std::map<char,Text*> labels;
  std::vector<Text*> temp_labels;
  
  // ranking
  std::vector<Record> records;
  int player_rank;
  std::string ranking_path;
  
  // timing
  unsigned int words_last_ticks;
  unsigned int colours_last_ticks;
  unsigned int clouds_last_ticks;

  // game data
  GameStatus gamestatus;
  bool debugmode;
  Data gamedata;
  std::vector<TextEntity*> words;
  std::string keybuffer;
  Difficulty game_diff; // game difficulty
  Record player;
  int lives;
  int cleared_wrds; // cleared words used for level & boss
  int combo; // combo counter
  bool lastcorr; // was last character correct?
  bool bossmode; // is boss mode
  bool falling; // are new words falling

  // stats
  int typedchrs;
  int corrchrs;
  int typedwrds; // typed words used for stats
  unsigned int start_type_ticks;
  unsigned int typing_ticks;
  unsigned int registered_type;
  unsigned int start_game_ticks;
  int alphabet[26];
  
};

#endif // _SDLENGINE_H_

