#
# Makefile
#


# compilatore da usare
CPP = g++

# opzioni da passare al compilatore
CPPFLAGS = -std=c++11

# nome dell'eseguibile
EXE = tinytextgame

# lista dei file header separati dagli spazi
HDRS = sdlengine.h data.h text.h fps.h animation.h textentity.h cloud.h

# lista delle librerie da utilizzare separate dagli spazi
# ogni libreria dovrà avere il prefisso -l
LIBS = -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lSDL2_gfx -lfontconfig

# lista dei file oggetto separati dagli spazi
OBJS = main.o data.o text.o animation.o fps.o textentity.o cloud.o sdlengine.o

# default
$(EXE): $(OBJS) $(HDRS) Makefile
	$(CPP) $(CPPFLAGS) -o $@ $(OBJS) $(LIBS)

# dipendenze 
main.o : main.cc sdlengine.h
	$(CPP) $(CPPFLAGS) -c main.cc

data.o : data.cc data.h
	$(CPP) $(CPPFLAGS) -c data.cc

text.o : text.cc text.h
	$(CPP) $(CPPFLAGS) -c text.cc

animation.o : animation.cc animation.h
	$(CPP) $(CPPFLAGS) -c animation.cc

fps.o : fps.cc fps.h
	$(CPP) $(CPPFLAGS) -c fps.cc

textentity.o : textentity.cc textentity.h animation.h fps.h
	$(CPP) $(CPPFLAGS) -c textentity.cc

cloud.o : cloud.cc cloud.h animation.h fps.h
	$(CPP) $(CPPFLAGS) -c cloud.cc

sdlengine.o : sdlengine.cc sdlengine.h data.h text.h textentity.h cloud.h fps.h
	$(CPP) $(CPPFLAGS) -c sdlengine.cc

# pulizia
.PHONY: clean
clean:
	rm -f core $(EXE) *.o

# installazione
install:
	mkdir -p $(DESTDIR)/usr/bin
	install -m 0755 $(EXE) $(DESTDIR)/usr/bin/$(EXE)
	mkdir -p $(DESTDIR)/usr/share/$(EXE)/sounds
	install -m 0664 sounds/*.wav $(DESTDIR)/usr/share/$(EXE)/sounds
	install -m 0664 sounds/*.mp3 $(DESTDIR)/usr/share/$(EXE)/sounds
	install -m 0664 dictionary.ita.txt $(DESTDIR)/usr/share/$(EXE)
	install -m 0644 $(EXE).desktop $(DESTDIR)/usr/share/applications
	install -m 0644 icons/48x48/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/48x48/apps
	install -m 0644 icons/64x64/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/64x64/apps
	install -m 0644 icons/72x72/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/72x72/apps
	install -m 0644 icons/96x96/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/96x96/apps
	install -m 0644 icons/128x128/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/128x128/apps
	install -m 0644 icons/192x192/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/192x192/apps
	install -m 0644 icons/256x256/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/256x256/apps
	install -m 0644 icons/512x512/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/512x512/apps
	gtk-update-icon-cache /usr/share/icons/hicolor

# disinstallazione
uninstall:
	rm -f $(DESTDIR)/usr/bin/$(EXE)
	rm -f -R $(DESTDIR)/usr/share/$(EXE)
	rm -f $(DESTDIR)/usr/share/applications/$(EXE).desktop
	rm -f $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/64x64/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/72x72/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/96x96/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/192x192/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/256x256/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/512x512/apps/$(EXE).png
