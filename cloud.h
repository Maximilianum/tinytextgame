/*
 * cloud.h
 * Copyright (C) 2019 Massimiliano Maniscalco
 *
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CLOUD_H_
#define _CLOUD_H_

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <vector>
#include "animation.h"

#define MAX_SPEED 1.0f
#define CLOUD_DELAY_MIN 3000
#define CLOUD_DELAY_MAX 9000
#define WHITE  { 0xff,0xff,0xff,0xff }
#define RED   { 0xff,0x5f,0x5f,0xff }
#define TO_RED true
#define TO_WHITE false

class Cloud {
 public:
  Cloud(SDL_Renderer* rndr, SDL_Colour clr, unsigned int alp);
  ~Cloud();
  inline void set_destination(const SDL_Rect* rct) { destination = *rct; };
  inline SDL_Rect get_destination(void) const { return destination; }
  inline bool is_animating(void) const { return animating; }
  void draw(void) const;
  void on_loop(void);
  void move(float spd);
  void start_colour_animation(bool flag);
  bool animate_colour(void);

 protected:
  void rebuild_texture(SDL_Colour clr);
  static SDL_Texture* create_texture(SDL_Renderer* rndr, SDL_Colour clr, SDL_Rect* rct, std::vector<SDL_Rect>* ellipses);

  static int width;
  static int height;
  
  SDL_Renderer* renderer;
  SDL_Texture* texture;
  SDL_Colour colour;
  SDL_Rect source;
  SDL_Rect destination;
  std::vector<SDL_Rect> ellipses;
  int alpha;
  float speed;
  float acceleration;
  Animation animation;
  bool to_red;
  bool animating;
  
};

#endif // _CLOUD_H_
