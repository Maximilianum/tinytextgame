/*
 * main.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 *
 * This file is part of TinyTextGame
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "sdlengine.h"

int main(int argc, char* argv[])
{
	int result = -1;
	std::string path ("/usr/share/tinytextgame/");
	bool debugmode = false;
	if (argc > 1) {
	  for (int x = 1; x < argc; x++) {
	    std::string arg (argv[x]);
	    if (arg.compare(std::string("-l")) == 0 || arg.compare(std::string("--local")) == 0) {
	      path.assign("");
	    } else if (arg.compare(std::string("-d")) == 0 || arg.compare(std::string("--debug")) == 0) {
	      debugmode = true;
	    } else {
	      std::cerr << "unknown argument: " << arg << std::endl;
	    }
	  }
	}
	SDLEngine *engine = new SDLEngine(path, debugmode);

    if (engine)
    {
        result = engine->start();
        delete engine;
    }
	
	return result;
}

