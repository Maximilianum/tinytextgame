/*
 * animation.h
 * Copyright (C) 2019 Massimiliano Maniscalco
 *
 * This file is part of TinyTextGame
 * 
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include <SDL2/SDL.h>

class Animation {
 public:
 Animation(void) : current_frame (0), old_time (0) {}
  Animation(int tf, int fr);
  inline void set_current_frame(int val) { current_frame = val; }
  inline int get_current_frame(void) const { return current_frame; }
  inline void set_total_frames(int val) { total_frames = val; }
  inline int get_total_frames(void) const { return total_frames; }
  inline void set_frame_rate(int val) { frame_rate = val; }
  inline int get_frame_rate(void) const { return frame_rate; }
  bool animate(void);
  
 private:
  int current_frame;
  int total_frames;
  int frame_rate;
  unsigned int old_time;
  
};

#endif // _ANIMATION_H_
