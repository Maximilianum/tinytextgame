/*
 * textentity.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "textentity.h"
#include <chrono>
#include <random>
#include <cmath>
#include <sstream>
#include "fps.h"

TextEntity::TextEntity(SDL_Renderer* rndr, const std::string& str, TTF_Font* fnt1, TTF_Font* fnt2, SDL_Colour clr0, SDL_Colour clr1) : font1 (fnt1), font2 (fnt2), std_colour (clr0), mtc_colour (clr1), speed (0.0f), acceleration (0.0f), falling (false), matched (0) {
  text = new Text(rndr, str, font1, std_colour);
  animation.set_total_frames(str.size() + 1);
  animation.set_frame_rate(100);
}

TextEntity::~TextEntity() {
  if (text) {
    delete text;
  }
}

void TextEntity::set_matched(int val) {
  int previous = matched;
  matched = val;
  if (previous != matched) {
    text->update_texture(matched, font2, mtc_colour);
  }
}

void TextEntity::on_loop(void) {
  if (!falling) {
    speed = 0.0f;
    acceleration = 0.0f;
  } else {
    acceleration = 0.5f;
  }

  speed += acceleration * Fps::control.get_speed_factor();

  if (speed > MAX_SPEED) {
    speed = MAX_SPEED;
  }

  move(speed);
}

void TextEntity::move(float spd) {
  if (spd == 0.0f) {
    return;
  }

  spd *= Fps::control.get_speed_factor();
  SDL_Rect dest = text->get_destination();
  dest.y += round(spd);
  text->set_destination(&dest);
}

void TextEntity::set_random_position_x(int min, int max) {
  SDL_Rect dest = text->get_destination();
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_int_distribution<int> distribution(min, (max - dest.w));
  dest.x = distribution(generator);
  dest.y = 0;
  text->set_destination(&dest);
}

char TextEntity::check_string(const std::string& str) {
  /* return one of te following values:
   * 0 = perfect match
   * -1 = partly match
   * 1 = doens't match */
  if (matched < text->get_string().size()) {
    int previous = matched;
    if (str.size() <= text->get_string().size()) {
      if (text->get_string().substr(0,str.size()).compare(str) == 0) {
	set_matched(str.size());
	return str.size() == text->get_string().size() ? 0 : -1;
      } else if (str.size() > 1 && text->get_string().substr(0,str.size() - 1).compare(str.substr(0, str.size() - 1)) == 0) {
	return 1;
      }
    }
    set_matched(0);
  }
  return 1;
}

bool TextEntity::animate_del(void) {
  if (animation.animate()) {
    int cf = animation.get_current_frame();
    std::string word = text->get_string();
    std::stringstream ss;
    if (cf <= word.size()) {
      char c = word.at(cf - 1) >='A' && word.at(cf - 1) <= 'Z' ? word.at(cf - 1) + 32 : '#'; // +32 change character to lowercase
      ss << std::string(cf - 1, ' ') << c << word.substr(cf);
    }
    text->update_texture(ss.str(), mtc_colour);
    return (cf - 1) < word.size();
  }
  return true;
}

char TextEntity::get_char_at_index(int index) {
  std::string str = text->get_string();
  if (index < str.size()) {
    return str.at(index);
  }
  return -1;
}
