/*
 * sdlengine.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdlengine.h"
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include "fps.h"

/*
 * Initialization
 */
bool SDLEngine::init(void) {
  if (debugmode) {
    std::cout << "game files path: " << main_path << std::endl;
  }
  
  // inizializza SDL2
  if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    on_SDL_error(std::cerr, "SDL_Init");
    return false;
  }

  // init SDL_image
  if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
    on_SDL_error(std::cerr, "IMG_Init");
    return false;
  }

  // init SDL2_ttf
  if (TTF_Init() != 0) {
    on_SDL_error(std::cerr, "TTF_Init");
    return false;
  }

  // load gamefont
  FcChar8 rtype[] = "Regular";
  std::string fpath = find_font_path(std::string("LiberationMono"), rtype);
  if ((gamefont = TTF_OpenFont(fpath.c_str(), GAME_FONTSIZE)) == nullptr) {
    on_SDL_error(std::cerr, "TTF_OpenFont");
    return false;
  }

  // load boldfont
  FcChar8 btype[] = "Bold";
  fpath = find_font_path(std::string("LiberationMono"), btype);
  if ((boldfont = TTF_OpenFont(fpath.c_str(), GAME_FONTSIZE)) == nullptr) {
    on_SDL_error(std::cerr, "TTF_OpenFont");
    return false;
  }
  
  // load bigfont
  if ((bigfont = TTF_OpenFont(fpath.c_str(), BIG_FONTSIZE)) == nullptr) {
    on_SDL_error(std::cerr, "TTF_OpenFont");
    return false;
  }
    
  // init SDL2_mixer
  if ((Mix_Init(MIX_INIT_OGG) & MIX_INIT_OGG) != MIX_INIT_OGG) {
    on_SDL_error(std::cerr, "Mix_Init");
    return false;
  }

  // initialize Mixer API
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096) != 0) {
    on_SDL_error(std::cerr, "Mix_OpenAudio");
    return false;
  }
  
  // crea una finestra
  if((window = SDL_CreateWindow("Tiny Text Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, win_width, win_height, SDL_WINDOW_OPENGL)) == nullptr) {
    on_SDL_error(std::cerr, "SDL_CreateWindow");
    return false;
  }

  // crea un'istanza di SDL_Renderer
  if((renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE)) == nullptr) {
    on_SDL_error(std::cerr, "SDL_CreateRenderer");
    return false;
  }

  gamedata.load_dictionary(main_path + std::string("dictionary.ita.txt"));
  if (debugmode) {
    std::cout << gamedata.get_debug_info() << std::endl;
  }
  load_ranking();
  SDL_StopTextInput();
  std::string path = main_path + SOUNDS_PATH + std::string("robobozo.mp3");
  game_music = Mix_LoadMUS(path.c_str());
  if (game_music == nullptr) {
    on_SDL_error(std::cerr, "Load music");
    gamestatus = GameStatus::quit;
  } else {
    if (Mix_PlayMusic(game_music, -1) != 0) {
      on_SDL_error(std::cerr, "Play music");
    }
  }
  init_title();
    
  return true;
}

SDLEngine::~SDLEngine() {

  free_labels();
  free_temp_labels();
  free_words();
  free_clouds();
  free_sounds();
  Mix_FreeMusic(game_music);

  TTF_CloseFont(gamefont);
  TTF_CloseFont(boldfont);
  TTF_CloseFont(bigfont);

  if (renderer)
    SDL_DestroyRenderer(renderer);

  if (window)
    SDL_DestroyWindow(window);

  Mix_CloseAudio();
  Mix_Quit();
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

int SDLEngine::start(void) {
  if(init() == false)
    return -1;
 
  SDL_Event event;
 
  while(gamestatus != GameStatus::quit) {
    while(SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
	gamestatus = GameStatus::quit;
      }
      switch(gamestatus) {
      case GameStatus::title:
	on_title_event(&event);
	break;
      case GameStatus::credits:
	on_credits_event(&event);
	break;
      case GameStatus::game:
	on_game_event(&event);
	break;
      case GameStatus::pause:
	on_pause_event(&event);
	break;
      case GameStatus::report:
	on_report_event(&event);
	break;
      case GameStatus::ranking:
	on_ranking_event(&event);
	break;
      }
    }

    switch(gamestatus) {
    case GameStatus::title:
      labels.at(LABEL_CREDITS)->update_fade();
      labels.at(LABEL_START)->update_fade();
      update_colours();
      break;
    case GameStatus::credits:
      labels.at(LABEL_ESC)->update_fade();
      break;
    case GameStatus::game:
      Fps::control.on_loop();
      update_temp_labels();
      update_words();
      update_clouds();
      break;
    case GameStatus::pause:
      update_temp_labels();
      break;
    case GameStatus::gameover:
      on_gameover_loop();
      break;
    case GameStatus::report:
      update_colours();
      break;
    }
		
    switch(gamestatus) {
    case GameStatus::title:
      title_render();
      break;
    case GameStatus::credits:
      credits_render();
      break;
    case GameStatus::game:
    case GameStatus::pause:
    case GameStatus::gameover:
      game_render();
      break;
    case GameStatus::report:
      report_render();
      break;
    case GameStatus::ranking:
      ranking_render();
      break;
    }
  }

  Mix_HaltMusic();
  
  return 0;
}

void SDLEngine::on_SDL_error(std::ostream &os, const std::string &msg) {
  os << msg << " error: " << SDL_GetError() << std::endl;
}

void SDLEngine::on_gameover_loop(void) {
  std::vector<Text*>::iterator itl = temp_labels.begin();
  while (itl != temp_labels.end()) {
    if (*itl != nullptr) {
      (*itl)->update_fade();
      if (!(*itl)->get_fade()) {
	delete *itl;
	*itl = nullptr;
	itl = temp_labels.erase(itl);
      } else {
	itl++;
      }
    } else {
      itl++;
    }
  }
  if (temp_labels.size() == 0) {
    gamestatus = GameStatus::report;
    free_memory();
    init_report();
  }
}

void SDLEngine::on_title_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::quit;
    } else if (event->key.keysym.sym == SDLK_LSHIFT) {
      gamestatus = GameStatus::credits;
      free_memory();
      init_credits();
    } else if (event->key.keysym.sym == SDLK_SPACE) {
      Fps::control.reset();
      gamestatus = GameStatus::game;
      free_memory();
      cleanup_data();
      init_game();
      start_game_ticks = SDL_GetTicks();
    }
  } 
}

void SDLEngine::on_credits_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::title;
      free_memory();
      init_title();
    }
  } 
}

void SDLEngine::on_game_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    std::stringstream ss;
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::report;
      free_memory();
      init_report();
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      gamestatus = GameStatus::pause;
    } else if (event->key.keysym.sym >= 'a' && event->key.keysym.sym <= 'z') {
      typedchrs++;
      char c = event->key.keysym.sym - 32; // -32 change character to capital letter
      ss << keybuffer << c;
      check_words(ss.str());
    } else if ((event->key.keysym.sym >= '0' && event->key.keysym.sym <= '9') || event->key.keysym.sym == ',' || event->key.keysym.sym == '.') {
      typedchrs++;
      char c = event->key.keysym.sym;
      ss << keybuffer << c;
      check_words(ss.str());
    } else if (event->key.keysym.sym == SDLK_TAB && debugmode) { // to help test and develop levels
      falling = false;
      cleared_wrds = game_diff.nextlevel;
    }
  } 
}

void SDLEngine::on_pause_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_RETURN || event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::game;
      words_last_ticks = SDL_GetTicks();
      clouds_last_ticks = SDL_GetTicks();
      Fps::control.on_loop();
    }
  }
}

void SDLEngine::on_report_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::quit;
    } else if (event->key.keysym.sym == SDLK_SPACE) {
      gamestatus = GameStatus::ranking;
      free_memory();
      if (init_ranking()) {
	SDL_StartTextInput();
      }
    }
  }
}

void SDLEngine::on_ranking_event(SDL_Event* event) {
  if (event->type == SDL_TEXTINPUT) {
    if (player.name.size() < MAX_PLAYER_NAME) {
      std::string chr = event->text.text;
      if (chr == std::string(" ")) {
	chr = std::string("_");
      }
      player.name.append(chr);
      if (player_rank > 0) {
	Text* lbl = labels.at(player_rank);
	lbl->update_texture(create_record_string(player, player_rank), lbl->get_colour());
	std::vector<Record>::iterator itr = records.begin() + player_rank -1;
	itr->name = player.name;
      }
    }
  } else if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::quit;
    } else if (event->key.keysym.sym == SDLK_SPACE) {
      if (SDL_IsTextInputActive() == SDL_FALSE) {
	save_ranking();
	gamestatus = GameStatus::title;
	free_memory();
	init_title();
      }
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      SDL_StopTextInput();
    } else if (event->key.keysym.sym == SDLK_BACKSPACE) {
      if (SDL_IsTextInputActive() == SDL_TRUE && player.name.size() > 0) {
	player.name = player.name.substr(0,player.name.size() - 1);
	if (player_rank > 0) {
	  Text* lbl = labels.at(player_rank);
	  lbl->update_texture(create_record_string(player, player_rank), lbl->get_colour());
	  std::vector<Record>::iterator itr = records.begin() + player_rank -1;
	  itr->name = player.name;
	}
      }
    }
  }
}

void SDLEngine::title_render(void) {
  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
  SDL_Colour col = colours_cycle.at(0).first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderClear(renderer);
  float xr = win_width / 51.0f;
  float yr = win_height / 40.0f;
  draw_T(1.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 1);
  draw_i(9.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 7);
  draw_n(17.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 12);
  draw_y(25.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 20);
  draw_T(17.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 27);
  draw_e(25.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 34);
  draw_x(33.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 41);
  draw_t(41.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 49);
  draw_G(9.0f, 18.0f, &xr, &yr, colours_cycle.begin() + 57);
  draw_a(17.0f, 18.0f, &xr, &yr, colours_cycle.begin() + 65);
  draw_m(25.0f, 18.0f, &xr, &yr, colours_cycle.begin() + 72);
  draw_e(33.0f, 18.0f, &xr, &yr, colours_cycle.begin() + 81);

  std::map<char,Text*>::iterator itl = labels.begin();
  while (itl != labels.end()) {
    if (itl->second) {
      itl->second->draw();
    }
    itl++;
  }
  
  SDL_RenderPresent(renderer);
}

void SDLEngine::credits_render(void) {
  SDL_SetRenderDrawColor(renderer,0,0,0,255);
  SDL_RenderClear(renderer);

  std::map<char,Text*>::iterator itl = labels.begin();
  while (itl != labels.end()) {
    if (itl->second) {
      itl->second->draw();
    }
    itl++;
  }
  
  SDL_RenderPresent(renderer);
}

void SDLEngine::game_render(void) {
  const GameColours* gcols = gamedata.get_game_colours(player.level);
  SDL_SetRenderDrawColor(renderer, gcols->sky.r * 0.33f, gcols->sky.g * 0.33f, gcols->sky.b * 0.33f, gcols->sky.a * 0.5f);
  SDL_RenderClear(renderer);
  for (short i = 0; i < 65; i++) {
    SDL_Rect rect = { 0,GAME_FONTSIZE + (i * (win_height / 64)),win_width,win_height / 64 };
    SDL_SetRenderDrawColor(renderer,gcols->sky.r - (2 * i),gcols->sky.g - (2 * i),gcols->sky.b - (2 * i),gcols->sky.a);
    SDL_RenderFillRect(renderer,&rect);
  }
  
  std::map<char,Text*>::iterator itl = labels.begin();
  while (itl != labels.end()) {
    if (itl->second) {
      itl->second->draw();
    }
    itl++;
  }
  
  std::vector<TextEntity*>::iterator iter = words.begin();
  while (iter != words.end()) {
    if (*iter != nullptr) {
      (*iter)->draw();
    }
    iter++;
  }

  std::vector<Cloud*>::iterator itc = clouds.begin();
  while (itc != clouds.end()) {
    if (*itc != nullptr) {
      (*itc)->draw();
    }
    itc++;
  }
  
  std::vector<Text*>::iterator ittl = temp_labels.begin();
  while (ittl != temp_labels.end()) {
    if (*ittl) {
      (*ittl)->draw();
    }
    ittl++;
  }
  
  SDL_RenderPresent(renderer);
}

void SDLEngine::report_render(void) {
  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
  SDL_Colour col = colours_cycle.at(0).first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderClear(renderer);
  float xr = win_width / 51.0f;
  float yr = win_height / 40.0f;
  draw_G(9.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 1);
  draw_a(17.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 8);
  draw_m(25.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 15);
  draw_e(33.0f, 2.0f, &xr, &yr, colours_cycle.begin() + 24);
  draw_O(9.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 31);
  draw_v(17.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 39);
  draw_e(25.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 46);
  draw_r(33.0f, 10.0f, &xr, &yr, colours_cycle.begin() + 53);

  std::map<char,Text*>::iterator itl = labels.begin();
  while (itl != labels.end()) {
    if (itl->second) {
      itl->second->draw();
    }
    itl++;
  }
  
  SDL_RenderPresent(renderer);
}

void SDLEngine::ranking_render(void) {
  SDL_SetRenderDrawColor(renderer,0,0,0,255);
  SDL_RenderClear(renderer);

  std::map<char,Text*>::iterator itl = labels.begin();
  while (itl != labels.end()) {
    if (itl->second) {
      itl->second->draw();
    }
    itl++;
  }
  
  SDL_RenderPresent(renderer);
}

std::string SDLEngine::find_font_path(const std::string& name, const FcChar8* type) {
  std::string path;
  FcConfig* config = FcInitLoadConfigAndFonts();

  // configure the search pattern, 
  FcPattern* pat = FcNameParse((const FcChar8*)(name.c_str()));
  FcPatternAddString(pat,FC_STYLE,type); // set font style
  if (debugmode) {
    FcPatternPrint(pat); // print FcPattern for debugging
  }
  FcConfigSubstitute(config, pat, FcMatchPattern);
  FcDefaultSubstitute(pat);

  // find the font
  FcResult result;
  FcPattern* font = FcFontMatch(config, pat, &result);
  if (font) {
      FcChar8* file = nullptr;
      if (FcPatternGetString(font, FC_FILE, 0, &file) == FcResultMatch) {
	path.assign(std::string((char*)file));
      }
      FcPatternDestroy(font);
  }
  FcPatternDestroy(pat);
  if (debugmode) {
    std::cout << path << std::endl;
  }
  return path;
}

void SDLEngine::cycle_colours(void) {
  for (int n = 1; n < colours_cycle.size(); n++) {
    if (colours_cycle.at(n).second) {
      if (colours_cycle.at(n).first.r == 0xff) {
	if (colours_cycle.at(n).first.g == 0x90) {
	  colours_cycle.at(n).second = false;
	  colours_cycle.at(n).first.g -= 0x10;
	  colours_cycle.at(n).first.b -= 0x05;
	} else {
	  colours_cycle.at(n).first.g += 0x10;
	  colours_cycle.at(n).first.b += 0x05;
	}
      } else if (colours_cycle.at(n).first.g == 0xff) {
	if (colours_cycle.at(n).first.b == 0xbd) {
	  colours_cycle.at(n).second = false;
	  colours_cycle.at(n).first.b -= 0x15;
	  colours_cycle.at(n).first.r -= 0x05;
	} else {
	  colours_cycle.at(n).first.b += 0x15;
	  colours_cycle.at(n).first.r += 0x05;
	}
      } else {
	if (colours_cycle.at(n).first.g == 0x90) {
	  colours_cycle.at(n).second = false;
	  colours_cycle.at(n).first.g -= 0x10;
	  colours_cycle.at(n).first.r -= 0x05;
	} else {
	  colours_cycle.at(n).first.g += 0x10;
	  colours_cycle.at(n).first.r += 0x05;
	}
      }
    } else {
      if (colours_cycle.at(n).first.r == 0xff) {
	if (colours_cycle.at(n).first.g == 0x00) {
	  colours_cycle.at(n).second = true;
	  colours_cycle.at(n).first.g += 0x10;
	  colours_cycle.at(n).first.b += 0x05;
	} else {
	  colours_cycle.at(n).first.g -= 0x10;
	  colours_cycle.at(n).first.b -= 0x05;
	}
      } else if (colours_cycle.at(n).first.g == 0xff) {
	if (colours_cycle.at(n).first.b == 0x00) {
	  colours_cycle.at(n).second = true;
	  colours_cycle.at(n).first.b += 0x15;
	  colours_cycle.at(n).first.r += 0x05;
	} else {
	  colours_cycle.at(n).first.b -= 0x15;
	  colours_cycle.at(n).first.r -= 0x05;
	}
      } else {
	if (colours_cycle.at(n).first.g == 0x00) {
	  colours_cycle.at(n).second = true;
	  colours_cycle.at(n).first.g += 0x10;
	  colours_cycle.at(n).first.r += 0x05;
	} else {
	  colours_cycle.at(n).first.g -= 0x10;
	  colours_cycle.at(n).first.r -= 0x05;
	}
      }
    }
  }
}

void SDLEngine::init_title(void) {
  Text* lbl = new Text(renderer, "press <left shift> for credits", gamefont, { 190,255,190,255 });
  SDL_Rect dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.75f;
  lbl->set_destination(&dest);
  lbl->set_random_fade_start();
  lbl->set_fade(true);
  labels.insert(std::pair<char,Text*>(LABEL_CREDITS,lbl));

  lbl = new Text(renderer, "press <space> to start", gamefont, { 190,255,190,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.9f;
  lbl->set_destination(&dest);
  lbl->set_random_fade_start();
  lbl->set_fade(true);
  labels.insert(std::pair<char,Text*>(LABEL_START,lbl));

  colours_cycle.clear();
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0x00,0x00,0xff },false));

  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x00,0x00,0xff },true)); // T
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x10,0x05,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x20,0x0a,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x30,0x0f,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x40,0x14,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x50,0x19,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x60,0x1e,0xff },false)); // i
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x70,0x23,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x80,0x28,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x90,0x2d,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x80,0x28,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x70,0x23,0xff },true)); // n
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x60,0x1e,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x50,0x19,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x40,0x14,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x30,0x0f,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x20,0x0a,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x10,0x05,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x00,0x00,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x10,0x05,0xff },false)); // y
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x20,0x0a,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x30,0x0f,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x40,0x14,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x50,0x19,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x60,0x1e,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x70,0x23,0xff },false));
  
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0xff,0x93,0xff },true)); // T
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0xff,0x7e,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0xff,0x69,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0xff,0x54,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0xff,0x3f,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0xff,0x2a,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0xff,0x15,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0xff,0x00,0xff },true)); // e
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0xff,0x15,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0xff,0x2a,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0xff,0x3f,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0xff,0x54,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0xff,0x69,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0xff,0x7e,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0xff,0x93,0xff },false)); // x
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0xff,0xa8,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x2d,0xff,0xbd,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0xff,0xa8,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0xff,0x93,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0xff,0x7e,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0xff,0x69,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0xff,0x54,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0xff,0x3f,0xff },true)); // t
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0xff,0x2a,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0xff,0x15,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0xff,0x00,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0xff,0x15,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0xff,0x2a,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0xff,0x3f,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0xff,0x54,0xff },false));
  
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0x60,0xff,0xff },false)); // G
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0x70,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0x80,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x2d,0x90,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0x80,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0x70,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0x60,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0x50,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0x40,0xff,0xff },true)); // a
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0x30,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0x20,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0x10,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0x00,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0x10,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0x20,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0x30,0xff,0xff },false)); // m
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0x40,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0x50,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0x60,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0x70,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0x80,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x2d,0x90,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0x80,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0x70,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0x60,0xff,0xff },true)); // e
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0x50,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0x40,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0x30,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0x20,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0x10,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0x00,0xff,0xff },true));
  
  colours_last_ticks = SDL_GetTicks();
}

void SDLEngine::init_credits(void) {
  Text* lbl = new Text(renderer, "Game Design", gamefont, { 255,50,50,255 });
  SDL_Rect dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.10f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_GAMEDESIGN, lbl));

  lbl = new Text(renderer, "Massimiliano Maniscalco", gamefont, { 255,100,100,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.15f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_GDN1,lbl));

  lbl = new Text(renderer, "Silvio Maniscalco", gamefont, { 255,150,150,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.19f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_GDN2,lbl));

  lbl = new Text(renderer, "Programming", gamefont, { 50,255,50,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.30f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_PROGRAMMING,lbl));

  lbl = new Text(renderer, "Massimiliano Maniscalco", gamefont, { 100,255,100,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.35f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_PN1,lbl));

  lbl = new Text(renderer, "Music", gamefont, { 50,50,255,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.46f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_MUSIC,lbl));

  lbl = new Text(renderer, "\"Robobozo\" Kevin MacLeod (incompetech.com)", gamefont, { 100,100,255,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.51f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_MC1,lbl));

  lbl = new Text(renderer, "Licensed under Creative Commons: By Attribution 4.0 License", gamefont, { 150,150,255,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.55f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_MC2,lbl));

  lbl = new Text(renderer, "http://creativecommons.org/licenses/by/4.0/", gamefont, { 200,200,255,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.59f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_MC3,lbl));

  lbl = new Text(renderer, "press <esc> to go back", gamefont, { 255,255,255,255 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.9f;
  lbl->set_destination(&dest);
  lbl->set_random_fade_start();
  lbl->set_fade(true);
  labels.insert(std::pair<char,Text*>(LABEL_ESC,lbl));
}

void SDLEngine::init_game(void) {
  std::stringstream ss;
  SDL_Colour white = { 255,255,255,255 };
  ss << "LIVES " << lives;
  Text* lbl = new Text(renderer, ss.str(), gamefont, white);
  SDL_Rect dest = lbl->get_destination();
  dest.x = 10;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_LIVES,lbl));

  ss.str(std::string());
  ss << "SCORE " << std::setfill('0') << std::setw(5) << player.score;
  lbl = new Text(renderer, ss.str(), gamefont, white);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.95f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_SCORE,lbl));

  update_level();
  
  std::string path = main_path + SOUNDS_PATH + std::string("key_click.wav");
  Mix_Chunk* s = Mix_LoadWAV(path.c_str());
  if (!s) {
    on_SDL_error(std::cerr, "Loud sound");
    gamestatus = GameStatus::quit;
  } else {
    Mix_VolumeChunk(s, 128);
    sounds.push_back(s);
  }

  path = main_path + SOUNDS_PATH + std::string("word_cleared.wav");
  s = Mix_LoadWAV(path.c_str());
  if (!s) {
    on_SDL_error(std::cerr, "Loud sound");
    gamestatus = GameStatus::quit;
  } else {
    Mix_VolumeChunk(s, 200);
    sounds.push_back(s);
  }

  path = main_path + SOUNDS_PATH + std::string("mistake.wav");
  s = Mix_LoadWAV(path.c_str());
  if (!s) {
    on_SDL_error(std::cerr, "Loud sound");
    gamestatus = GameStatus::quit;
  } else {
    Mix_VolumeChunk(s, 200);
    sounds.push_back(s);
  }

  path = main_path + SOUNDS_PATH + std::string("next_level.wav");
  s = Mix_LoadWAV(path.c_str());
  if (!s) {
    on_SDL_error(std::cerr, "Loud sound");
    gamestatus = GameStatus::quit;
  } else {
    Mix_VolumeChunk(s, 200);
    sounds.push_back(s);
  }

  words_last_ticks = SDL_GetTicks();
  clouds_last_ticks = SDL_GetTicks();
  next_cloud_delay = CLOUD_DELAY_MIN;
}

void SDLEngine::init_report(void) {
  unsigned int game_elapsed_ticks = SDL_GetTicks() - start_game_ticks;
  std::stringstream ss;
  ss << "SCORE " << player.score;
  SDL_Colour col = { 230,255,230,255 };
  Text* lbl = new Text(renderer, ss.str(), gamefont, col);
  SDL_Rect dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.5f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_SCORE,lbl));
  
  ss.str(std::string()); // clean up stringstream
  ss << "LEVEL " << player.level;
  col = { 210,255,210,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.54f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_LEVEL,lbl));
  
  ss.str(std::string()); // clean up stringstream
  ss << "typed words: " << typedwrds;
  col = { 190,255,190,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.58f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_TYPED_WORD,lbl));
  
  ss.str(std::string()); // clean up stringstream
  ss << "typed characters: " << typedchrs << "   mistakes: " << typedchrs - corrchrs;
  col = { 170,255,170,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.62f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_TYPED_CHAR,lbl));
  
  ss.str(std::string()); // clean up stringstream
  if (typedchrs > 0) {
    char fbuffer[6];
    snprintf(fbuffer, 6, "%2.2f", corrchrs / (typedchrs / 100.0f));
    ss << "accuracy: " << fbuffer << "%";
  } else {
    ss << "accuracy: --";
      }
  col = { 150,255,150,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.66f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_ACCURACY,lbl));
  
  ss.str(std::string()); // clean up stringstream
  float seconds = typing_ticks / 1000.0f;
  float minutes = game_elapsed_ticks / 60000.0f;
  float typedps = registered_type / seconds;
  float typedpm = registered_type / minutes;
  ss << "characters per second: ";
  if (seconds > 1.0f) {
    char fbuffer[6];
    snprintf(fbuffer, 6, "%2.2f", typedps);
    ss << fbuffer;
  } else {
    ss << "--";
  }
  ss << ", per minute: ";
  if (minutes > 1.0f) {
    char fbuffer[6];
    snprintf(fbuffer, 6, "%2.2f", typedpm);
    ss << fbuffer;
  } else {
    ss << "--";
  }
  col = { 130,255,130,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.7f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_TYPE_SPEED,lbl));
  
  ss.str(std::string()); // clean up stringstream
  ss << "words per minute: ";
  if (minutes > 1.0f) {
    char fbuffer[6];
    player.wordspermin = typedwrds / minutes;
    snprintf(fbuffer, 6, "%2.2f", player.wordspermin);
    ss << fbuffer;
  } else {
    player.wordspermin = 0.0f;
    ss << "--";
  }
  col = { 110,255,110,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.74f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_WORDS_PM,lbl));
  
  ss.str(std::string()); // clean up stringstream
  ss << "best combo: " << player.bestcombo;
  col = { 90,255,90,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.78f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_BEST_COMBO,lbl));
  
  ss.str(std::string()); // clean up stringstream
  std::string worst, best;
  get_letters_stats(&worst, &best);
  ss << "worst letters: " << worst;
  col = { 70,255,70,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.82f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_WORST_LTR,lbl));
  
  ss.str(std::string()); // clean up stringstream
  ss << "best letters: " << best;
  col = { 50,255,50,255 };
  lbl = new Text(renderer, ss.str(), gamefont, col);
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = win_height * 0.86f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_BEST_LTR,lbl));

  colours_cycle.clear();
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0x00,0x00,0xff },false));
  
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x00,0x00,0xff },true)); // G
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x10,0x05,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x20,0x0a,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x30,0x0f,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x40,0x14,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x50,0x19,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x60,0x1e,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x70,0x23,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x80,0x28,0xff },false)); // a
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x90,0x2d,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x80,0x28,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x70,0x23,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x60,0x1e,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x50,0x19,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x40,0x14,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x30,0x0f,0xff },true)); // m
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x20,0x0a,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x10,0x05,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x00,0x00,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x10,0x05,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x20,0x0a,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x30,0x0f,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x40,0x14,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x50,0x19,0xff },false)); // e
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x60,0x1e,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x70,0x23,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x80,0x28,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x90,0x2d,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x80,0x28,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0xff,0x70,0x23,0xff },true));

  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0x00,0xff,0xff },true)); // O
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0x10,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0x20,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0x30,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0x40,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0x50,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0x60,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0x70,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0x80,0xff,0xff },false)); // v
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x2d,0x90,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0x80,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0x70,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0x60,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0x50,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0x40,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0x30,0xff,0xff },true)); // e
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0x20,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0x10,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x00,0x00,0xff,0xff },true));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x05,0x10,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0a,0x20,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x0f,0x30,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x14,0x40,0xff,0xff },false)); // r
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x19,0x50,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x1e,0x60,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x23,0x70,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x28,0x80,0xff,0xff },false));
  colours_cycle.push_back(std::pair<SDL_Colour,bool>({ 0x2d,0x90,0xff,0xff },false));
}

bool SDLEngine::init_ranking(void) {
  Text* lbl = new Text(renderer, "TOP TEN PLAYERS", bigfont, { 0x00,0x90,0xff,0xff });
  SDL_Rect dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.01f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_RANKING,lbl));

  lbl = new Text(renderer, "         NAME         SCORE LEVEL COMBO  WPM", gamefont, { 0x00,0xa8,0xf0,0xe7 });
  dest = lbl->get_destination();
  dest.x = (win_width - dest.w) / 2;
  dest.y = win_height * 0.3f;
  lbl->set_destination(&dest);
  labels.insert(std::pair<char,Text*>(LABEL_RANKING_TOP,lbl));

  // update ranking
  player_rank = 0;
  int n = 1;
  std::vector<Record>::iterator iter = records.begin();
  while (iter != records.end()) {
    if (player.score > iter->score) {
      player_rank = n;
      records.emplace(iter, player);
      iter = records.end() - 1;
      iter = records.erase(iter);
    } else {
      iter++;
      n++;
    }
  }

  // create Text objects
  float y = 0.35f;
  SDL_Colour col = { 0x00, 0xa8, 0xf0, 0xe7 };
  n = 1;
  iter = records.begin();
  while (iter != records.end()) {
    Text* txt = new Text(renderer, create_record_string(*iter, n), gamefont, col);
    SDL_Rect dest = txt->get_destination();
    dest.x = win_width * 0.25f;
    dest.y = win_height * y;
    txt->set_destination(&dest);
    labels.insert(std::pair<char,Text*>(n,txt));
    y += 0.04f;
    col.r += 0x18;
    col.g -= 0x0f;
    col.b -= 0x18;
    n++;
    iter++;
  }
  return player_rank > 0;
}

std::string SDLEngine::create_record_string(const Record& r, int n) {
  std::stringstream ss;
  std::string name;
  char fbuffer[6];
  if (r.name.size() > MAX_PLAYER_NAME) {
    name = r.name.substr(0,MAX_PLAYER_NAME);
  } else if (r.name.size() < MAX_PLAYER_NAME) {
    name = r.name + std::string(MAX_PLAYER_NAME - r.name.size(), ' ');
  } else {
    name = r.name;
  }
  snprintf(fbuffer, 6, "%2.2f", r.wordspermin);
  ss << std::setfill(' ') << std::setw(2) << n << ". " << name << "   " << std::setfill('0') << std::setw(5) << r.score <<  "  " << std::setfill(' ') << std::setw(2) << r.level << "   " << std::setfill(' ') << std::setw(3) << r.bestcombo << "   " << fbuffer;
  return ss.str();
}

void SDLEngine::load_ranking(void) {
  struct passwd* pw = getpwuid(getuid());
  std::string homepath = std::string(pw->pw_dir);
  std::string dirpath = homepath;
  dirpath.append("/.local/share/tinytextgame");
  ranking_path = dirpath;
  ranking_path.append("/rankings");
  struct stat buffer;
  int result = stat(ranking_path.c_str(),&buffer);
  if (result == 0) {
    std::ifstream infile (ranking_path, std::ifstream::in);
    while (infile.good()) {
      Record r;
      infile >> r.name;
      if (infile.good()) {
	infile >> r.score;
	if (infile.good()) {
	  infile >> r.level;
	  if (infile.good()) {
	    infile >> r.bestcombo;
	    if (infile.good()) {
	      infile >> r.wordspermin;
	      if (infile.good()) {
		records.push_back(r);
	      }
	    }
	  }
	}
      }
    }
    infile.close();
  } else {
    result = stat(dirpath.c_str(),&buffer);
    if (result != 0) {
      if(mkdir(dirpath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0) {
	std::cerr << "Couldn't create directory " << dirpath << std::endl;
      }
    }
    for (int n = 0; n < 10; n++) {
      Record r = { std::string("unknown"),3000 - (150 * n),10 - n,20 - n,40.0f - (2 * n) };
      records.push_back(r);
    }
  }
}

void SDLEngine::save_ranking(void) {
  // save ranking
  std::ofstream outfile (ranking_path, std::ofstream::out);
  if (outfile.good()) {
    std::vector<Record>::iterator iter = records.begin();
    while (iter != records.end()) {
      outfile << iter->name << " " << iter->score << " " << iter->level << " " << iter->bestcombo << " " << iter->wordspermin << std::endl;
      iter++;
    }
  }
  outfile.close();
}

void SDLEngine::check_words(const std::string& str) {
  int prev_state;
  bool mistake = true;
  Mix_PlayChannel(CHANNEL_KEYBOARD,sounds.at(SOUND_KEY),0);
  std::vector<TextEntity*>::iterator iter = words.begin();
  while (iter != words.end()) {
    prev_state = (*iter)->get_matched();
    char r = (*iter)->check_string(str);
    switch (r) {
    case -1: // partly match
      mistake = false;
      break;
    case 0: // word completed
      {
	Mix_PlayChannel(CHANNEL_WORD,sounds.at(SOUND_CLEARED),0);
	corrchrs++;
	if (str.size() > 1) {
	  typedwrds++;
	}
	if (falling) {
	  cleared_wrds++;
	}
	if (game_diff.bossmode > 0 && cleared_wrds == game_diff.bossmode) {
	  falling = false;
	  bossmode = true;
	}
	if (cleared_wrds == game_diff.nextlevel) {
	  falling = false;
	  if (bossmode) {
	    bossmode = false;
	    std::vector<Cloud*>::iterator iter = clouds.begin();
	    while (iter != clouds.end()) {
	      (*iter)->start_colour_animation(TO_WHITE);
	      iter++;
	    }
	  }
	}
	lastcorr = true;
	if (str.size() > 1) {
	  typing_ticks += SDL_GetTicks() - start_type_ticks;
	  registered_type += str.size();
	}
	if (lastcorr && str.size() > 1) {
	  combo++;
	  if (combo > player.bestcombo) {
	    player.bestcombo = combo;
	  }
	}
	int scorecheck = player.score / 10000.0f;
	player.score = combo > 1 ? player.score + (str.size() * combo) : player.score + str.size();
	int newscore = player.score / 10000.0f;
	if (newscore > scorecheck) {
	  lives++;
	  update_lives();
	  const GameColours* gcols = gamedata.get_game_colours(player.level);
	  std::stringstream ss;
	  ss << "*1 life";
	  Text* txt = new Text(renderer, ss.str(), bigfont, gcols->score);
	  SDL_Rect dest = txt->get_destination();
	  dest.x = (win_width - dest.w) / 2;
	  dest.y = win_height * 0.45f;
	  txt->set_destination(&dest);
	  txt->set_fade_mode(FadeMode::inout);
	  txt->set_fade(true);
	  temp_labels.push_back(txt);
	}
	update_score();
	keybuffer.assign("");
	std::vector<TextEntity*>::iterator it = words.begin();
	while (it != words.end()) {
	  if (*iter != *it) {
	    (*it)->set_matched(0);
	  }
	  it++;
	}
	reset_words_status(0);
	return;
      }
    case 1: // not match
      if (keybuffer.size() > 0 && prev_state == keybuffer.size()) {
	alphabet[(*iter)->get_char_at_index(prev_state) - 'A']++; // increase one error for the expected letter
    }
      break;
    }
    iter++;
  }
  if (mistake) {
    if (debugmode) {
      std::cout << "WRONG... " << str << std::endl;
    }
    Mix_PlayChannel(CHANNEL_MISTAKE,sounds.at(SOUND_MISTAKE),0);
    lastcorr = false;
    combo = 0;
  } else {
    corrchrs++;
    keybuffer = str;
    if (keybuffer.size() == 1) {
      start_type_ticks = SDL_GetTicks();
    }
    reset_words_status(str.size());
  }
}

void SDLEngine::reset_words_status(int size) {
  std::vector<TextEntity*>::iterator iter = words.begin();
  while (iter != words.end()) {
    if (!(*iter)->is_cleared() && (*iter)->get_matched() > 0) {
      if (size == 0 || (*iter)->get_matched() < size) {
	(*iter)->set_matched(0);
      }
    }
    iter++;
  }
}

void SDLEngine::create_score_temp_label(TextEntity* txe) {
  const GameColours* gcols = gamedata.get_game_colours(player.level);
  std::stringstream ss;
  ss << "+";
  if (combo > 1 && txe->get_size() > 1) {
    ss << txe->get_size() * combo;
  } else {
    ss << txe->get_size();
  }
  Text* txt = new Text(renderer, ss.str(), gamefont, gcols->score);
  SDL_Rect dest = txe->get_destination();
  SDL_Rect dest2 = txt->get_destination();
  dest2.x = dest.x - ((dest2.w - dest.w) / 2);
  dest2.y = dest.y;
  txt->set_destination(&dest2);
  txt->set_fade_mode(FadeMode::inout);
  txt->set_fade(true);
  temp_labels.push_back(txt);
  if (combo > 1 && txe->get_size() > 1) {
    ss.str(std::string());
    ss << "combo " << combo;
    txt = new Text(renderer, ss.str(), gamefont, gcols->score);
    dest2 = txt->get_destination();
    dest2.x = dest.x - ((dest2.w - dest.w) / 2);
    dest2.y = dest.y + dest.h;
    txt->set_destination(&dest2);
    txt->set_fade_mode(FadeMode::inout);
    txt->set_fade(true);
    temp_labels.push_back(txt);
  }
}

void SDLEngine::update_colours(void) {
  if (colours_last_ticks + COLOURS_CYCLE_DELAY > SDL_GetTicks()) {
    return;
  }
  colours_last_ticks = SDL_GetTicks();
  cycle_colours();
}

void SDLEngine::update_temp_labels(void) {
  std::vector<Text*>::iterator itl = temp_labels.begin();
  while (itl != temp_labels.end()) {
    if (*itl != nullptr) {
      (*itl)->update_fade();
      if (!(*itl)->get_fade()) {
	delete *itl;
	*itl = nullptr;
	itl = temp_labels.erase(itl);
      } else {
	itl++;
      }
    } else {
      itl++;
    }
  }
}

void SDLEngine::update_words(void) {
  if (falling && words.size() < game_diff.maxwrds) {
    if (words_last_ticks + game_diff.falldelay < SDL_GetTicks()) {
      words_last_ticks = SDL_GetTicks();
      const GameColours* gcols = gamedata.get_game_colours(player.level);
      std::string str = gamedata.pop();
      if (str != "") {
	TextEntity* word = new TextEntity(renderer,str,gamefont,boldfont,gcols->word,gcols->match);
	if (word) {
	  word->set_random_position_x(0, win_width);
	  word->set_falling(true);
	  words.push_back(word);
	}
      } else if (words.size() == 0) {
	  gamestatus = GameStatus::report;
	  free_memory();
	  init_report();
	  return;
      }
    }
  }

  if (!falling && words.size() == 0) {
    if (bossmode) {
      falling = true;
      start_boss_mode();
    } else {
      falling = true;
      Mix_PlayChannel(CHANNEL_JINGLE,sounds.at(SOUND_NEXTLEV),0);
      player.level++;
      update_level();
    }
  }
  
  std::vector<TextEntity*>::iterator iter = words.begin();
  while (iter != words.end()) {
    if (*iter != nullptr) {
      (*iter)->on_loop();
      SDL_Rect dest = (*iter)->get_destination();
      if (dest.y >= (win_height - dest.h)) {
	if (!(*iter)->is_cleared()) {
	  lost_life((*iter)->get_string(), dest.x + (dest.w / 2));
	  iter = words.end();
	} else {
	  delete *iter;
	  *iter = nullptr;
	  iter = words.erase(iter);
	}
      } else {
	if ((*iter)->is_cleared()) {
	  if (!(*iter)->animate_del()) {
	    create_score_temp_label(*iter);
	    delete *iter;
	    *iter = nullptr;
	    iter = words.erase(iter);
	  } else {
	    iter++;
	  }
	} else {
	  iter++;
	}
      }
    } else {
      iter++;
    }
  }
}

void SDLEngine::update_clouds(void) {
  if (clouds_last_ticks + next_cloud_delay < SDL_GetTicks()) {
    clouds_last_ticks = SDL_GetTicks();
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> delay(CLOUD_DELAY_MIN, CLOUD_DELAY_MAX);
    next_cloud_delay = delay(generator);
    SDL_Colour white = WHITE;
    SDL_Colour red = RED;
    SDL_Colour clr = bossmode && falling ? red : white;
    Cloud* cld = new Cloud(renderer, clr, 0x80);
    clouds.push_back(cld);
  }

  std::vector<Cloud*>::iterator iter = clouds.begin();
  while (iter != clouds.end()) {
    (*iter)->on_loop();
    SDL_Rect dest = (*iter)->get_destination();
    if (dest.x > win_width) {
      delete *iter;
      *iter = nullptr;
      iter = clouds.erase(iter);
    } else {
      iter++;
    }
  }
}

void SDLEngine::update_score(void) {
  Text* lbl = labels.at(LABEL_SCORE);
  std::stringstream ss;
  ss << "SCORE " << std::setfill('0') << std::setw(5) << player.score;
  SDL_Colour white = { 255,255,255,255 };
  lbl->rebuild_texture(ss.str(), white);
  SDL_Rect dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.95f;
  dest.y = 0;
  lbl->set_destination(&dest);
}

void SDLEngine::update_lives(void) {
  Text* lbl = labels.at(LABEL_LIVES);
  std::stringstream ss;
  ss << "LIVES " << lives;
  SDL_Colour white = { 255,255,255,255 };
  lbl->rebuild_texture(ss.str(), white);
  SDL_Rect dest = lbl->get_destination();
  dest.x = 10;
  lbl->set_destination(&dest);
}

void SDLEngine::get_letters_stats(std::string* worst, std::string* best) {
  int wl = 0;
  int bl = 0;
  std::stringstream ws, bs;
  for (int n = 0; n < 26; n++) {
    char c = 'A' + n;
    if (alphabet[n] > wl) {
      wl = alphabet[n];
      ws.str(std::string());
      ws << c << " ";
    } else if (alphabet[n] == wl) {
      ws << c << " ";
    }
    if (alphabet[n] < bl) {
      bl = alphabet[n];
      bs.str(std::string());
      bs << c << " ";
    } else if (alphabet[n] == bl) {
      bs << c << " ";
    }
  }
  worst->assign(ws.str());
  best->assign(bs.str());
}

void SDLEngine::update_level(void) {
  Difficulty gd = gamedata.get_difficulty(player.level);
  if (game_diff.length < gd.length) {
    gamedata.push(gd.length);
  }
  game_diff = gd;
  if (game_diff.length > 2) {
    int old_size = gamedata.size();
    gamedata.clear(game_diff.length - 2);
    if (debugmode) {
      int current_size = gamedata.size();
      std::cout << "removing " << game_diff.length - 2 << " characters long words" << std::endl;
      std::cout << "dropped " << current_size - old_size << " words." << std::endl;
      std::cout << "words available = " << current_size << std::endl;
    }
  }
  std::stringstream ss;
  ss << "LEVEL " << player.level;
  SDL_Colour white = { 255,255,255,255 };
  Text* lbl = nullptr;
  std::map<char,Text*>::iterator iter = labels.find(LABEL_LEVEL);
  if (iter != labels.end()) {
    lbl = iter->second;
    lbl->rebuild_texture(ss.str(), white);
  } else {
    lbl = new Text(renderer, ss.str(), gamefont, white);
    labels.insert(std::pair<char,Text*>(LABEL_LEVEL,lbl));
  }
  SDL_Rect dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.4f;
  lbl->set_destination(&dest);
  Text* levelbiglabel = new Text(renderer, ss.str(), bigfont, { 0,255,128,255 });
  dest = levelbiglabel->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = (win_height - dest.h) * 0.45f;
  levelbiglabel->set_destination(&dest);
  levelbiglabel->set_fade_mode(FadeMode::inout);
  levelbiglabel->set_fade(true);
  temp_labels.push_back(levelbiglabel);
}

void SDLEngine::start_boss_mode(void) {
  std::stringstream ss;
  ss << "WARNING!";
  Text* lbl = new Text(renderer, ss.str(), bigfont, { 255,0,0,255 });
  SDL_Rect dest = lbl->get_destination();
  dest.x = (win_width - dest.w) * 0.5f;
  dest.y = (win_height - dest.h) * 0.45f;
  lbl->set_destination(&dest);
  lbl->set_fade_mode(FadeMode::loop);
  lbl->set_loop_count(5);
  lbl->set_fade(true);
  temp_labels.push_back(lbl);
  game_diff.falldelay -= game_diff.falldelay * 0.25f;
  game_diff.maxwrds = game_diff.nextlevel - game_diff.bossmode;

  std::vector<Cloud*>::iterator iter = clouds.begin();
  while (iter != clouds.end()) {
    (*iter)->start_colour_animation(TO_RED);
    iter++;
  }
}

void SDLEngine::lost_life(const std::string& str, int x) {
  const GameColours* gcols = gamedata.get_game_colours(player.level);
  lives--;
  update_lives();
  std::stringstream ss;
  ss << str;
  Text* txt = new Text(renderer, ss.str(), bigfont, gcols->word);
  SDL_Rect dest = txt->get_destination();
  dest.x = x - (dest.w / 2);
  dest.y = win_height - (dest.h * 0.8);
  txt->set_destination(&dest);
  txt->set_fade_mode(FadeMode::inout);
  txt->set_fade(true);
  temp_labels.push_back(txt);
  if (lives == 0) {
    gamestatus = GameStatus::gameover;
    free_words();
    std::stringstream ss;
    ss << "Game Over";
    Text* txt = new Text(renderer, ss.str(), bigfont, gcols->score);
    SDL_Rect dest = txt->get_destination();
    dest.x = (win_width - dest.w) / 2;
    dest.y = win_height * 0.45f;
    txt->set_destination(&dest);
    txt->set_fade_mode(FadeMode::inout);
    txt->set_fade(true);
    temp_labels.push_back(txt);
  } else {
    keybuffer.assign("");
    reset_words_status(0);
    free_words();    
  }
}

void SDLEngine::cleanup_data(void) {
  keybuffer.assign("");
  game_diff = { 0,0,0,-1,0 };
  player = { std::string(), 0, 1, 0, 0.0f };
  lives = 3;
  cleared_wrds = 0;
  combo = 0;
  lastcorr = false;
  bossmode = false;
  falling = true;
  typedchrs = 0;
  corrchrs = 0;
  typedwrds = 0;
  typing_ticks = 0;
  registered_type = 0;
  start_game_ticks = 0;
  for (int n = 0; n < 26; n++) {
    alphabet[n] = 0;
  }
  
  gamedata.clear();
}

void SDLEngine::free_memory(void) {
  free_labels();
  switch (gamestatus) {
    //case GameStatus::title:
    //break;
    //case GameStatus::credits:
    //break;
    //case GameStatus::game:
    //break;
  case GameStatus::report:
    free_temp_labels();
    free_words();
    free_clouds();
    free_sounds();
    break;
    //case GameStatus::ranking:
    //break;
  }
}

void SDLEngine::free_labels(void) {
  std::map<char,Text*>::iterator iterl = labels.begin();
  while (iterl != labels.end()) {
    delete iterl->second;
    iterl++;
  }
  labels.clear();
}

void SDLEngine::free_temp_labels(void) {
  std::vector<Text*>::iterator iterl = temp_labels.begin();
  while (iterl != temp_labels.end()) {
    delete *iterl;
    iterl++;
  }
  temp_labels.clear();
}

void SDLEngine::free_words(void) {
  std::vector<TextEntity*>::iterator iter = words.begin();
  while (iter != words.end()) {
    delete *iter;
    iter++;
  }
  words.clear();
}

void SDLEngine::free_sounds(void) {
  std::vector<Mix_Chunk*>::iterator iter = sounds.begin();
  while (iter != sounds.end()) {
    Mix_FreeChunk(*iter);
    iter++;
  }
  sounds.clear();
}

void SDLEngine::free_clouds(void) {
  std::vector<Cloud*>::iterator iterc = clouds.begin();
  while (iterc != clouds.end()) {
    delete *iterc;
    iterc++;
  }
  clouds.clear();
}

void SDLEngine::calculate_coord(int* xc, int* yc, const float* xr, const float* yr, const float* x, const float* y) {
  for (int i = 0; i < 9; i++) {
    xc[i] = (int)std::round(*xr * (*x + i));
    yc[i] = (int)std::round(*yr * (*y + i));
  }
}

void SDLEngine::draw_T(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[0], xc[2] - xc[1], yc[1] - yc[0] },
		      { xc[2], yc[0], xc[3] - xc[2], yc[1] - yc[0] },
		      { xc[3], yc[0], xc[4] - xc[3], yc[2] - yc[0] },
		      { xc[4], yc[0], xc[5] - xc[4], yc[2] - yc[0] },
		      { xc[3], yc[2], xc[4] - xc[3], yc[4] - yc[2] },
		      { xc[5], yc[0], xc[6] - xc[5], yc[1] - yc[0] },
		      { xc[4], yc[2], xc[5] - xc[4], yc[4] - yc[2] },
		      { xc[3], yc[4], xc[4] - xc[3], yc[6] - yc[4] },
		      { xc[6], yc[0], xc[7] - xc[6], yc[1] - yc[0] },
		      { xc[4], yc[4], xc[5] - xc[4], yc[6] - yc[4] },
		      { xc[3], yc[6], xc[4] - xc[3], yc[7] - yc[6] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  SDL_RenderFillRect(renderer,&rect[10]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[11]);
}

void SDLEngine::draw_i(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[3], yc[1], xc[4] - xc[3], yc[2] - yc[1] },
		      { xc[2], yc[3], xc[3] - xc[2], yc[4] - yc[3] },
		      { xc[4], yc[1], xc[5] - xc[4], yc[2] - yc[1] },
		      { xc[3], yc[3], xc[4] - xc[3], yc[4] - yc[3] },
		      { xc[4], yc[3], xc[5] - xc[4], yc[4] - yc[3] },
		      { xc[3], yc[4], xc[4] - xc[3], yc[6] - yc[4] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[4], yc[4], xc[5] - xc[4], yc[6] - yc[4] },
		      { xc[3], yc[6], xc[4] - xc[3], yc[7] - yc[6] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  SDL_RenderFillRect(renderer,&rect[1]);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[2]);
  SDL_RenderFillRect(renderer,&rect[3]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[4]);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[7]);
  SDL_RenderFillRect(renderer,&rect[8]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[9]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[10]);
}

void SDLEngine::draw_n(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[2], xc[2] - xc[1], yc[4] - yc[2] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[6] - yc[4] },
		      { xc[3], yc[2], xc[4] - xc[3], yc[3] - yc[2] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[1], yc[6], xc[2] - xc[1], yc[7] - yc[6] },
		      { xc[4], yc[2], xc[5] - xc[4], yc[3] - yc[2] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[6], yc[3], xc[7] - xc[6], yc[4] - yc[3] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[6] - yc[4] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[6] - yc[4] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] },
		      { xc[6], yc[6], xc[7] - xc[6], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  SDL_RenderFillRect(renderer,&rect[5]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[9]);
  SDL_RenderFillRect(renderer,&rect[10]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[11]);
  SDL_RenderFillRect(renderer,&rect[12]);
  col = (itcol + 7)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[13]);
}

void SDLEngine::draw_y(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[2], xc[2] - xc[1], yc[4] - yc[2] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[5] - yc[4] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[1], yc[7], xc[2] - xc[1], yc[8] - yc[7] },
		      { xc[3], yc[5], xc[4] - xc[3], yc[6] - yc[5] },
		      { xc[2], yc[7], xc[3] - xc[2], yc[8] - yc[7] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[4], yc[5], xc[5] - xc[4], yc[6] - yc[5] },
		      { xc[3], yc[7], xc[4] - xc[3], yc[8] - yc[7] },
		      { xc[6], yc[2], xc[7] - xc[6], yc[4] - yc[2] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[6] - yc[4] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[6] - yc[4] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[7]);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[10]);
  SDL_RenderFillRect(renderer,&rect[11]);
  SDL_RenderFillRect(renderer,&rect[12]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[13]);
  SDL_RenderFillRect(renderer,&rect[14]);
}

void SDLEngine::draw_e(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[3], xc[2] - xc[1], yc[4] - yc[3] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[6] - yc[4] },
		      { xc[3], yc[2], xc[4] - xc[3], yc[3] - yc[2] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[4], yc[2], xc[5] - xc[4], yc[3] - yc[2] },
		      { xc[3], yc[4], xc[4] - xc[3], yc[5] - yc[4] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[4], yc[4], xc[5] - xc[4], yc[5] - yc[4] },
		      { xc[3], yc[6], xc[4] - xc[3], yc[7] - yc[6] },
		      { xc[6], yc[3], xc[7] - xc[6], yc[4] - yc[3] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[5] - yc[4] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[5] - yc[4] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  SDL_RenderFillRect(renderer,&rect[10]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[11]);
  SDL_RenderFillRect(renderer,&rect[12]);
  SDL_RenderFillRect(renderer,&rect[13]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[14]);
  SDL_RenderFillRect(renderer,&rect[15]);
}

void SDLEngine::draw_x(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[2], xc[2] - xc[1], yc[3] - yc[2] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[3], yc[3], xc[4] - xc[3], yc[4] - yc[3] },
		      { xc[2], yc[5], xc[3] - xc[2], yc[6] - yc[5] },
		      { xc[1], yc[6], xc[2] - xc[1], yc[7] - yc[6] },
		      { xc[4], yc[3], xc[5] - xc[4], yc[4] - yc[3] },
		      { xc[3], yc[4], xc[4] - xc[3], yc[6] - yc[4] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[4], yc[4], xc[5] - xc[4], yc[6] - yc[4] },
		      { xc[6], yc[2], xc[7] - xc[6], yc[3] - yc[2] },
		      { xc[5], yc[5], xc[6] - xc[5], yc[6] - yc[5] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] },
		      { xc[6], yc[6], xc[7] - xc[6], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[2]);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[10]);
  SDL_RenderFillRect(renderer,&rect[11]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[12]);
  col = (itcol + 7)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[13]);
}

void SDLEngine::draw_t(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[2], xc[2] - xc[1], yc[3] - yc[2] },
		      { xc[3], yc[1], xc[4] - xc[3], yc[2] - yc[1] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[3] - yc[2] },
		      { xc[4], yc[1], xc[5] - xc[4], yc[2] - yc[1] },
		      { xc[3], yc[2], xc[4] - xc[3], yc[4] - yc[2] },
		      { xc[4], yc[2], xc[5] - xc[4], yc[4] - yc[2] },
		      { xc[3], yc[4], xc[4] - xc[3], yc[6] - yc[4] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[3] - yc[2] },
		      { xc[4], yc[4], xc[5] - xc[4], yc[6] - yc[4] },
		      { xc[6], yc[2], xc[7] - xc[6], yc[3] - yc[2] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] },
		      { xc[6], yc[6], xc[7] - xc[6], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[7]);
  SDL_RenderFillRect(renderer,&rect[8]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[9]);
  SDL_RenderFillRect(renderer,&rect[10]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[11]);
  col = (itcol + 7)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[12]);
}

void SDLEngine::draw_G(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[1], xc[2] - xc[1], yc[2] - yc[1] },
		      { xc[2], yc[0], xc[3] - xc[2], yc[2] - yc[0] },
		      { xc[1], yc[2], xc[2] - xc[1], yc[4] - yc[2] },
		      { xc[3], yc[0], xc[4] - xc[3], yc[1] - yc[0] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[6] - yc[4] },
		      { xc[4], yc[0], xc[5] - xc[4], yc[1] - yc[0] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[5], yc[0], xc[6] - xc[5], yc[2] - yc[0] },
		      { xc[4], yc[3], xc[5] - xc[4], yc[4] - yc[3] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[6], yc[1], xc[7] - xc[6], yc[2] - yc[1] },
		      { xc[5], yc[3], xc[6] - xc[5], yc[4] - yc[3] },
		      { xc[3], yc[6], xc[4] - xc[3], yc[7] - yc[6] },
		      { xc[6], yc[3], xc[7] - xc[6], yc[4] - yc[3] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[6] - yc[4] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[6] - yc[4] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  SDL_RenderFillRect(renderer,&rect[5]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  SDL_RenderFillRect(renderer,&rect[10]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[11]);
  SDL_RenderFillRect(renderer,&rect[12]);
  SDL_RenderFillRect(renderer,&rect[13]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[14]);
  SDL_RenderFillRect(renderer,&rect[15]);
  SDL_RenderFillRect(renderer,&rect[16]);
  col = (itcol + 7)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[17]);
  SDL_RenderFillRect(renderer,&rect[18]);
}

void SDLEngine::draw_a(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[2], yc[2], xc[3] - xc[2], yc[3] - yc[2] },
		      { xc[1], yc[5], xc[2] - xc[1], yc[6] - yc[5] },
		      { xc[3], yc[2], xc[4] - xc[3], yc[3] - yc[2] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[4], yc[2], xc[5] - xc[4], yc[3] - yc[2] },
		      { xc[3], yc[4], xc[4] - xc[3], yc[5] - yc[4] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[4], yc[4], xc[5] - xc[4], yc[5] - yc[4] },
		      { xc[3], yc[6], xc[4] - xc[3], yc[7] - yc[6] },
		      { xc[6], yc[3], xc[7] - xc[6], yc[4] - yc[3] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[6] - yc[4] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[6] - yc[4] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] },
		      { xc[6], yc[6], xc[7] - xc[6], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  SDL_RenderFillRect(renderer,&rect[1]);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[2]);
  SDL_RenderFillRect(renderer,&rect[3]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[4]);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[7]);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[10]);
  SDL_RenderFillRect(renderer,&rect[11]);
  SDL_RenderFillRect(renderer,&rect[12]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[13]);
  SDL_RenderFillRect(renderer,&rect[14]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[15]);
}

void SDLEngine::draw_m(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[2], xc[2] - xc[1], yc[4] - yc[2] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[6] - yc[4] },
		      { xc[3], yc[3], xc[4] - xc[3], yc[4] - yc[3] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[1], yc[6], xc[2] - xc[1], yc[7] - yc[6] },
		      { xc[4], yc[3], xc[5] - xc[4], yc[4] - yc[3] },
		      { xc[3], yc[4], xc[4] - xc[3], yc[5] - yc[4] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[4], yc[4], xc[5] - xc[4], yc[6] - yc[4] },
		      { xc[6], yc[2], xc[7] - xc[6], yc[4] - yc[2] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[5] - yc[4] },
		      { xc[7], yc[3], xc[8] - xc[7], yc[4] - yc[3] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[6] - yc[4] },
		      { xc[7], yc[4], xc[8] - xc[7], yc[6] - yc[4] },
		      { xc[6], yc[6], xc[7] - xc[6], yc[7] - yc[6] },
		      { xc[7], yc[6], xc[8] - xc[7], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  SDL_RenderFillRect(renderer,&rect[5]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  SDL_RenderFillRect(renderer,&rect[8]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[9]);
  SDL_RenderFillRect(renderer,&rect[10]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[11]);
  SDL_RenderFillRect(renderer,&rect[12]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[13]);
  SDL_RenderFillRect(renderer,&rect[14]);
  col = (itcol + 7)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[15]);
  SDL_RenderFillRect(renderer,&rect[16]);
  col = (itcol + 8)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[17]);
}

void SDLEngine::draw_O(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[1], xc[2] - xc[1], yc[2] - yc[1] },
		      { xc[2], yc[0], xc[3] - xc[2], yc[2] - yc[0] },
		      { xc[1], yc[2], xc[2] - xc[1], yc[4] - yc[2] },
		      { xc[3], yc[0], xc[4] - xc[3], yc[1] - yc[0] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[6] - yc[4] },
		      { xc[4], yc[0], xc[5] - xc[4], yc[1] - yc[0] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[5], yc[0], xc[6] - xc[5], yc[2] - yc[0] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[6], yc[1], xc[7] - xc[6], yc[2] - yc[1] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[3], yc[6], xc[4] - xc[3], yc[7] - yc[6] },
		      { xc[6], yc[2], xc[7] - xc[6], yc[4] - yc[2] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[6] - yc[4] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[6] - yc[4] },
		      { xc[5], yc[6], xc[6] - xc[5], yc[7] - yc[6] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  SDL_RenderFillRect(renderer,&rect[5]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[10]);
  SDL_RenderFillRect(renderer,&rect[11]);
  SDL_RenderFillRect(renderer,&rect[12]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[13]);
  SDL_RenderFillRect(renderer,&rect[14]);
  SDL_RenderFillRect(renderer,&rect[15]);
  col = (itcol + 7)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[16]);
  SDL_RenderFillRect(renderer,&rect[17]);
}

void SDLEngine::draw_v(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[2], xc[2] - xc[1], yc[4] - yc[2] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[5] - yc[4] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[3], yc[5], xc[4] - xc[3], yc[6] - yc[5] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[4], yc[5], xc[5] - xc[4], yc[6] - yc[5] },
		      { xc[3], yc[6], xc[4] - xc[3], yc[7] - yc[6] },
		      { xc[6], yc[2], xc[7] - xc[6], yc[4] - yc[2] },
		      { xc[5], yc[4], xc[6] - xc[5], yc[6] - yc[4] },
		      { xc[4], yc[6], xc[5] - xc[4], yc[7] - yc[6] },
		      { xc[6], yc[4], xc[7] - xc[6], yc[5] - yc[4] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[4]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[5]);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  SDL_RenderFillRect(renderer,&rect[9]);
  SDL_RenderFillRect(renderer,&rect[10]);
  col = (itcol + 6)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[11]);
}

void SDLEngine::draw_r(float x, float y, const float* xr, const float* yr, std::vector<std::pair<SDL_Colour,bool>>::iterator itcol) {
  int xc[9];
  int yc[9];
  calculate_coord(xc, yc, xr, yr, &x, &y);
  SDL_Rect rect[] = { { xc[1], yc[2], xc[2] - xc[1], yc[4] - yc[2] },
		      { xc[2], yc[2], xc[3] - xc[2], yc[4] - yc[2] },
		      { xc[1], yc[4], xc[2] - xc[1], yc[6] - yc[4] },
		      { xc[3], yc[2], xc[4] - xc[3], yc[3] - yc[2] },
		      { xc[2], yc[4], xc[3] - xc[2], yc[6] - yc[4] },
		      { xc[1], yc[6], xc[2] - xc[1], yc[7] - yc[6] },
		      { xc[4], yc[2], xc[5] - xc[4], yc[3] - yc[2] },
		      { xc[2], yc[6], xc[3] - xc[2], yc[7] - yc[6] },
		      { xc[5], yc[2], xc[6] - xc[5], yc[4] - yc[2] },
		      { xc[6], yc[3], xc[7] - xc[6], yc[4] - yc[3] } };
  SDL_Colour col = itcol->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,rect);
  col = (itcol + 1)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[1]);
  SDL_RenderFillRect(renderer,&rect[2]);
  col = (itcol + 2)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[3]);
  SDL_RenderFillRect(renderer,&rect[4]);
  SDL_RenderFillRect(renderer,&rect[5]);
  col = (itcol + 3)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[6]);
  SDL_RenderFillRect(renderer,&rect[7]);
  col = (itcol + 4)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[8]);
  col = (itcol + 5)->first;
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderFillRect(renderer,&rect[9]);
}
