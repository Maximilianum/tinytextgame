/*
 * text.h
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TEXT_H_
#define _TEXT_H_

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#define GAME_FONTSIZE 25
#define BIG_FONTSIZE 80
#define FRAME_RATE 50

enum class TextFade { in, out };
enum class FadeMode { loop, inout };
  
class Text {
 public:
  Text(SDL_Renderer* rndr, const std::string& str, TTF_Font* fnt, SDL_Colour clr);
  ~Text();
  inline void set_destination(const SDL_Rect* rct) { destination = *rct; };
  inline SDL_Rect get_destination(void) const { return destination; }
  inline std::string get_string(void) const { return text; }
  inline void set_fade(bool flag) { fade = flag; }
  inline bool get_fade(void) const { return fade; }
  inline SDL_Colour get_colour(void) const { return colour; }
  inline void set_loop_count(int val) { loop_count = val; }
  void set_fade_mode(FadeMode mode);
  bool get_size(int* w, int* h);
  void draw() const;
  void set_random_fade_start(void);
  void update_fade(void);
  void update_texture(int matched, TTF_Font* fnt, const SDL_Colour& col);
  void update_texture(const std::string& str, const SDL_Colour& col);
  void rebuild_texture(const std::string& str, const SDL_Colour& col);

 protected:
  std::string text;
  SDL_Renderer* renderer;
  SDL_Texture* texture;
  SDL_Colour colour;
  SDL_Rect source;
  SDL_Rect destination;
  TTF_Font* font;
  bool fade;
  int fade_step;
  TextFade fade_verso;
  unsigned int last_tick;
  FadeMode fade_mode;
  int loop_count;
  int loop_counter;

 private:
  void create_texture(SDL_Texture** txtr, const std::string& str, const SDL_Colour& col);

};

#endif // _TEXT_H_
