/*
 * textentity.h
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TEXTENTITY_H_
#define _TEXTENTITY_H_

#include "text.h"
#include "animation.h"

#define MAX_SPEED 1.0f

class TextEntity {
 public:
  TextEntity(SDL_Renderer* rndr, const std::string& str, TTF_Font* fnt1, TTF_Font* fnt2, SDL_Colour clr0, SDL_Colour clr1);
  ~TextEntity();
  inline SDL_Rect get_destination(void) const { return text->get_destination(); }
  inline void set_destination(SDL_Rect* rect) { text->set_destination(rect); }
  inline void draw(void) const { text->draw(); }
  inline bool is_cleared(void) const { return matched == text->get_string().size(); }
  inline int get_size(void) const { return text->get_string().size(); }
  inline void set_falling(bool flag) { falling = flag; }
  inline int get_matched(void) const { return matched; }
  void set_matched(int val);
  void on_loop(void);
  void move(float spd);
  void set_random_position_x(int min, int max);
  char check_string(const std::string& str);
  bool animate_del(void);
  char get_char_at_index(int index);
  inline std::string get_string(void) const { return text->get_string(); }

 protected:
  TTF_Font* font1;
  TTF_Font* font2;
  Text* text;
  SDL_Colour std_colour;
  SDL_Colour mtc_colour;
  float speed;
  float acceleration;
  bool falling;
  int matched;
  Animation animation;
  
};

#endif // _TEXTENTITY_H_
