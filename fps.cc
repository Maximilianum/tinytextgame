/*
 * fps.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fps.h"

Fps Fps::control;

Fps::Fps() : old_time (SDL_GetTicks()), last_time (SDL_GetTicks()), speed_factor (((SDL_GetTicks() - last_time) / 1000.0f) * 32.0f) {
  num_frames = 0;
  frames = 0;
}

void Fps::reset(void) {
  old_time = SDL_GetTicks();
  last_time = SDL_GetTicks();
  num_frames = 0;
  frames = 0;
  speed_factor = 0;
}

void Fps::on_loop(void) {
  if (old_time + 1000 < SDL_GetTicks()) {
    old_time = SDL_GetTicks();
    num_frames = frames;
    frames = 0;
  }
  speed_factor = ((SDL_GetTicks() - last_time) / 1000.0f) * 32.0f;
  last_time = SDL_GetTicks();
  frames++;
}
