/*
 * animation.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 *
 * This file is part of TinyTextGame
 * 
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "animation.h"

Animation::Animation(int tf, int fr) : total_frames (tf), frame_rate (fr) {
  current_frame = 0;
  old_time = 0;
}

bool Animation::animate(void) {
  if (old_time + frame_rate > SDL_GetTicks()) {
    return false;
  }

  old_time = SDL_GetTicks();
  current_frame++;
  if (current_frame > total_frames) {
    current_frame = total_frames;
  }
  return true;
}
