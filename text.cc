/*
 * text.cc
 * Copyright (C) 2019 Massimiliano Maniscalco
 * 
 * This file is part of TinyTextGame
 *
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "text.h"
#include <chrono>
#include <random>

Text::Text(SDL_Renderer* rndr, const std::string& str, TTF_Font* fnt, SDL_Colour clr) : renderer (rndr), text (str), font (fnt), colour (clr), fade (false), fade_step (10), texture (nullptr), last_tick (SDL_GetTicks()), fade_verso (TextFade::out), fade_mode (FadeMode::loop), loop_count (-1), loop_counter (0) {
  create_texture(&texture, text, clr);
  if (!texture)
    {
      std::cerr << "SDL_CreateTextureFromSurface error: " << SDL_GetError() << std::endl;
      return;
    }
}

Text::~Text() {
  if (texture) {
    SDL_DestroyTexture(texture);
  }
}

void Text::set_fade_mode(FadeMode mode) {
  fade_mode = mode;
  switch(mode) {
  case FadeMode::loop:
    fade_step = 10;
    fade_verso = TextFade::out;
    break;
  case FadeMode::inout:
    fade_step = 0;
    fade_verso = TextFade::in;
    break;
  }
}

bool Text::get_size(int* w, int* h) {
  int result = SDL_QueryTexture(texture, nullptr, nullptr, w, h);
  return result == 0;
}

void Text::draw() const {
  SDL_Rect dest = destination;
  dest.x += (dest.w / 2) - (source.w / 2);
  dest.y += (dest.h / 2) - (source.h / 2);
  dest.w = source.w;
  dest.h = source.h;
  if (texture != nullptr) {
    if (fade)
      SDL_SetTextureAlphaMod(texture, (255 / 10) * fade_step);
    else
      SDL_SetTextureAlphaMod(texture, 255);
    SDL_RenderCopy(renderer, texture, &source, &dest);
  }
}

void Text::set_random_fade_start(void) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_int_distribution<int> distribution(0, 10);
  fade_step = distribution(generator);
}

void Text::update_fade(void) {
  if (last_tick + FRAME_RATE > SDL_GetTicks()) {
    return;
  }
  last_tick = SDL_GetTicks();
  switch (fade_verso) {
  case TextFade::in:
    fade_step++;
    if (fade_step > 10) {
      fade_step = 10;
      if (fade_mode == FadeMode::loop || fade_mode == FadeMode::inout) {
	fade_verso = TextFade::out;
      }
    }
    break;
  case TextFade::out:
    fade_step--;
    if (fade_step < 0) {
      fade_step = 0;
      if (fade_mode == FadeMode::loop) {
	fade_verso = TextFade::in;
	loop_counter++;
	if (loop_counter == loop_count) {
	  fade = false;
	}
      } else if (fade_mode == FadeMode::inout) {
	fade = false;
      }
    }
    break;
  }
}

void Text::update_texture(int matched, TTF_Font* fnt, const SDL_Colour& col) {
  if (matched > 0) {
    SDL_Surface *surf = TTF_RenderUTF8_Blended(fnt, text.substr(0, matched).c_str(), col);
    if (surf) {
      SDL_Texture* temp = SDL_CreateTextureFromSurface(renderer, surf);
      if (temp) {
	SDL_Rect src;
	src.x = 0;
	src.y = 0;
	SDL_QueryTexture(temp, NULL, NULL, &src.w, &src.h);
	SDL_Rect dst = src;
	if (texture) {
	  SDL_SetRenderTarget(renderer, texture);
	  SDL_RenderCopy(renderer, temp, &src, &dst);
	  SDL_SetRenderTarget(renderer, nullptr);
	}
	SDL_DestroyTexture(temp);
      }
      SDL_FreeSurface(surf);
    }
  } else {
    int x = destination.x;
    int y = destination.y;
    rebuild_texture(text, colour);
    destination.x = x;
    destination.y = y;
  }
}

void Text::update_texture(const std::string& str, const SDL_Colour& col) {
  if (texture) {
    SDL_SetRenderTarget(renderer, texture);
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(renderer, 0,0,0,0);
    SDL_RenderClear(renderer);
    SDL_SetRenderTarget(renderer, nullptr);
  }
  SDL_Surface *surf = nullptr;
  if (str.size() > 0) {
    colour = col;
    surf = TTF_RenderUTF8_Blended(font, str.c_str(), col);
    if (surf) {
      SDL_Texture* temp = SDL_CreateTextureFromSurface(renderer, surf);
      if (temp) {
	SDL_Rect src;
	src.x = 0;
	src.y = 0;
	SDL_QueryTexture(temp, NULL, NULL, &src.w, &src.h);
	SDL_Rect dst = src;
	if (texture) {
	  SDL_SetRenderTarget(renderer, texture);
	  SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	  SDL_RenderCopy(renderer, temp, &src, &dst);
	  SDL_SetRenderTarget(renderer, nullptr);
	}
	SDL_DestroyTexture(temp);
      }
      SDL_FreeSurface(surf);
    }
  }
}

void Text::rebuild_texture(const std::string& str, const SDL_Colour& col) {
  colour = col;
  create_texture(&texture, str, col);
}

void Text::create_texture(SDL_Texture** txtr, const std::string& str, const SDL_Colour& col)
{
  if (*txtr) {
    SDL_DestroyTexture(*txtr);
    *txtr = nullptr;
  }

  //We need to render to a surface as that's what TTF_RenderText
  //returns, then load that surface into a texture
  SDL_Surface *surf = TTF_RenderUTF8_Blended(font, str.c_str(), col);
  if (surf == nullptr)
    std::cerr << "TTF_RenderUTF8_Blended error: " << SDL_GetError() << std::endl;
	
  SDL_Texture* temp = SDL_CreateTextureFromSurface(renderer, surf);
  if (temp) {
    source.x = 0;
    source.y = 0;
    SDL_QueryTexture(temp, NULL, NULL, &source.w, &source.h);
    destination = source;
    *txtr = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, source.w, source.h);
    if (*txtr) {
      SDL_SetRenderTarget(renderer, *txtr);
      SDL_SetTextureBlendMode(*txtr, SDL_BLENDMODE_BLEND);
      SDL_SetRenderDrawColor(renderer, 0,0,0,0);
      SDL_RenderClear(renderer);
      SDL_RenderCopy(renderer, temp, &source, &destination);
      SDL_SetRenderTarget(renderer, nullptr);
    }
    SDL_DestroyTexture(temp);
  }
	
  //Clean up the surface
  SDL_FreeSurface(surf);
}
